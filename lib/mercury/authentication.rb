module Mercury
  module Authentication

    def can_edit?
      if current_user.try(:admin?)
        true
      else
        flash[:alert] = "devise.failure.invalid_role"
        redirect_to root_path
      end
    end

  end
end
