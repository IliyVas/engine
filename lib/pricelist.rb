# -*- encoding : utf-8 -*-
module Pricelist

	def self.create_category_node(category, xml, ids, parent = false)
		unless ids.include? category.id
			if Product.where(visible: true, category_id: category.id).exists? || parent
				ids.push category.id
				if category.prev_id
					xml.category(id: category.id, parentId: category.prev_id) { xml.text category.title }
					create_category_node(Category.find(category.prev_id), xml, ids, true)
				else
					xml.category(id: category.id) { xml.text category.title }
				end
			end
		end
	end

	def self.generate
		require 'nokogiri'

		builder = Nokogiri::XML::Builder.new(:encoding => 'UTF-8') do |xml|
			xml.doc.create_internal_subset('yml_catalog', nil, 'shops.dtd')
			xml.yml_catalog(date: Time.now.strftime("%Y-%m-%d %H:%M")) {
				xml.shop {
					xml.name "Двери возим"
					xml.company "Двери возим"
					xml.url "http://www.dveri-vozim.ru"
					xml.currencies {
						xml.currency(id: 'RUR', rate: "1")
					}
					xml.categories {
						ids = Array.new
						Category.where(special: false, hidden: false, last: true).each do |category|
							create_category_node(category, xml, ids)
						end
					}
					xml.local_delivery_cost "300"
					xml.offers {
						Product.where(visible: true).each do |product|
							product_presenter = Product::Presenter.new(product)

							xml.offer(id: product.id, type: "vendor.model", available: "true") {
								xml.url 'http://dveri-vozim.ru' + 
										Rails.application.routes.url_helpers.product_path(product.slug)
								xml.price product.current_price
								xml.currencyId "RUR"
								xml.categoryId product.category_id
								xml.picture 'http://dveri-vozim.ru' + product.basic_image.url(:normal)
								xml.typePrefix product.prefix
								xml.vendor product.vendor
								xml.model product.name
								xml.description product.short_description
								
								product_presenter.basic_features_without_options.each do |feat|
									if !product_presenter.feature_values(feat).text.blank?
										xml.param(name: feat.name) { xml.text product_presenter.last.text }
									end	
								end

							}
						end
					}
				}
			}
		end
		File.open(Rails.root.join('public','pricelist.xml'), 'w') { |f| f.write(builder.to_xml) }
		return builder.to_xml
	end
end