# -*- encoding : utf-8 -*-
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
GeneralContent.create(name: 'footer_left', content: '+7 (232) 231-52-34')
GeneralContent.create(name: 'footer_c', content: '+7 (232) 231-52-34')
GeneralContent.create(name: 'footer_center', content: '+7 (232) 231-52-34')
GeneralContent.create(name: 'footer_right', content: '+7 (232) 231-52-34')
GeneralContent.create(name: 'phone', content: '+7 (232) 231-52-34')
GeneralContent.create(name: 'business hours', content: 'Пн-Пт: 8:00-18:00, Сб: 8:00-15:00')
GeneralContent.create(name: 'email', content: 'store@store.ru')
GeneralContent.create(name: 'main_page_content', content: 'store@store.ru')

InfoPage.create(title: '<h2>Контакты</h2>' , info_category: 'Контакты', content: 'Контакты')
InfoPage.create(title: '<h2>Наши услуги</h2>', content: 'Наши услуги', info_category: 'Услуги', subcategory:'раздел "двери"')
InfoPage.create(title: '<h2>Как выбрать дверь</h2>', content: 'Как выбрать дверь', info_category: 'Информация', subcategory:'раздел "двери"')
User.create(email: 'admin@admin.ru', password: 'admin1', admin: true)
GeneralContent.create(name: 'body_top', content: '<h3>Популярные модели дверей</h3>')
GeneralContent.create(name: 'body_bottom', content: '<h4>Изменить текст</h4>')
Category.create(name: 'Межкомнатные двери', hidden: false, special: false, last: false)
Category.create(name: 'Стальные двери', hidden: false, special: false, last: false)
Category.create(name: 'Противопожарные двери', hidden: false, special: false, last: false)
Category.create(name: 'Раздвижные двери', hidden: false, special: false, last: false)
Category.create(name: 'Арки межкомнатные', hidden: false, special: false, last: false)
Category.create(name: 'Фурнитура для дверей', hidden: false, special: false, last: false)
Category.create(name: 'Монтажные материалы', hidden: false, special: false, last: false)
Category.create(name: 'Распродажа', hidden: false, special: true)
Category.create(name: 'Натуральные обои', hidden: false, special: false, last: false)
Category.create(name: 'Бамбуковое полотно (обои)', hidden: false, special: false, last: false)
c = Category.create(name: 'Шпонированные', prev_id: 1, last: true, order: 1, hidden: false, special: false)
Category.create(name: 'Экошпон', prev_id: 1, last: true, order: Category.maximum(:order)+1, hidden: false, special: false)

Category.create(name: 'Из массива', prev_id: 1, last: true, order: Category.maximum(:order)+1, hidden: false, special: false)
Category.create(name: 'Окрашенные (эмаль)', prev_id: 1, last: true, order: Category.maximum(:order)+1, hidden: false, special: false)

Category.create(name: 'ПВХ', prev_id: 1, last: true, order: Category.maximum(:order)+1, hidden: false, special: false)
Category.create(name: 'Ламинированные', prev_id: 1, last: true, order: Category.maximum(:order)+1, hidden: false, special: false)
Category.create(name: 'Офисные', prev_id: 1, last: true, order: Category.maximum(:order)+1, hidden: false, special: false)
Category.create(name: 'Строительные', prev_id: 1, last: true, order: Category.maximum(:order)+1, hidden: false, special: false)

Category.create(name: 'Стальные двери (Росссия)', prev_id: 2, last: true, order: Category.maximum(:order)+1, hidden: false, special: false)
Category.create(name: 'Стальные двери (КНР)', prev_id: 2, last: true, order: Category.maximum(:order)+1, hidden: false, special: false)

Category.create(name: 'Обои Hotey', prev_id: 9, last: true, order: Category.maximum(:order)+1, hidden: false, special: false)
Category.create(name: 'Обои RODEKA', prev_id: 9, last: true, order: Category.maximum(:order)+1, hidden: false, special: false)
Category.create(name: 'Обои Cosca', prev_id: 9, last: true, order: Category.maximum(:order)+1, hidden: false, special: false)
Category.create(name: 'Обои EL', prev_id: 9, last: true, order: Category.maximum(:order)+1, hidden: false, special: false)
Category.create(name: 'Обои Best', prev_id: 9, last: true, order: Category.maximum(:order)+1, hidden: false, special: false)
Category.create(name: 'Обои Pan-El', prev_id: 9, last: true, order: Category.maximum(:order)+1, hidden: false, special: false)
Category.create(name: 'Клей для натуральных обоев', prev_id: 9, last: true, order: Category.maximum(:order)+1, hidden: false, special: false)

Category.create(name: 'Из внешней части ствола бамбука', prev_id: 10, last: true, order: Category.maximum(:order)+1, hidden: false, special: false)
Category.create(name: 'Из внутренней части ствола бамбука', prev_id: 10, last: true, order: Category.maximum(:order)+1, hidden: false, special: false)
Category.create(name: 'Полотно COSCA A\'MIRO', prev_id: 10, last: true, order: Category.maximum(:order)+1, hidden: false, special: false)
Category.create(name: 'Обои (полотно) SAFAR', prev_id: 10, last: true, order: Category.maximum(:order)+1, hidden: false, special: false)
Category.create(name: 'Полотно комбинированное', prev_id: 10, last: true, order: Category.maximum(:order)+1, hidden: false, special: false)
Category.create(name: 'Полотно пресованное', prev_id: 10, last: true, order: Category.maximum(:order)+1, hidden: false, special: false)
Category.create(name: 'Клей для бамбукового полотна', prev_id: 10, last: true, order: Category.maximum(:order)+1, hidden: false, special: false)
Category.create(name: 'Планки бамбуковые', prev_id: 10, last: true, order: Category.maximum(:order)+1, hidden: false, special: false)

Category.create(name: 'Новинки', last: true, hidden: false, special: true)
Category.create(name: 'Акции', last: true, hidden: false, special: true) 
f = Array.new

f[1] = Feature.create(name:'Отделка', basic: true )
f[2] = Feature.create(name:'Каркас', basic: true )
f[3] = Feature.create(name:'Погонаж', basic: true )
f[4] = Feature.create(name:'Цвет', basic: true )
f[5] = Feature.create(name:'Внутреннее наполнение пототна', basic: false )
f[6] = Feature.create(name:'Багет', basic: true )
f[7] = Feature.create(name:'Остекление', basic: true )
f[8] = Feature.create(name:'Размер полотна', basic: true )
f[9] = Feature.create(name:'Ширина', basic: false )
f[10] = Feature.create(name:'Короб', basic: false )
f[11] = Feature.create(name:'Наличник', basic: false )
f[12] = Feature.create(name:'Упаковка двери', basic: false )

f[1].text_values.create(text: '1', picture_attributes: {image: File.open(Rails.root.join('1.jpg'))})
f[1].text_values.create(text: '2')

60.times {|i| Product.create(name: i, category_id: 11, basic_image:File.open(Rails.root.join('1.jpg')))}
=begin
#Сезия ГП
p = Product.new(name: 'Сезия итальянский орех ГП', category_id: 11, price: 1700, description: 'Ламинированная глухая дверь "Сезия" в цвете "итальянский орех".', basic_image: File.new(Rails.root.join('Doors images').join('Сезия Орех итальянский глухая.jpg')), on_main_page: true, visible: true)
p.features << Feature.all
t = f[1].text_values.create(text: 'финиш-пленка производства Германии')
p.text_values << t
t = f[2].text_values.create(text: 'брус дерева хвойных пород')
p.text_values << t
t = f[3].text_values.create(text: 'МДФ ламинированный')
p.text_values << t
t = f[4].pictures.create(image: File.new(Rails.root.join('Doors images').join('итальянский орех.jpg')))
p.pictures << t
t = f[5].text_values.create(text: 'сотовое')
p.text_values << t
t = f[6].text_values.create(text: 'ламинированный МДФ')
p.text_values << t
t = f[7].text_values.create(text: 'нет (глухое)')
p.text_values << t
t = f[8].text_values.create(text: '600 × 2000')
p.text_values << t
t = f[8].text_values.create(text: '700 × 2000')
p.text_values << t
t = f[8].text_values.create(text: '800 × 2000')
p.text_values << t
t = f[8].text_values.create(text: '900 × 2000')
p.text_values << t
t = f[9].text_values.create(text: '38мм')
p.text_values << t
t = f[10].text_values.create(text: 'короб прямой 30 × 70 × 2100 мм')
p.text_values << t
t = f[11].text_values.create(text: 'наличник полукруглый 10 × 70 × 2150 мм.')
p.text_values << t
t = f[12].text_values.create(text: 'полиэтилен, пенопласт')
p.text_values << t

p.complectations.build(name: 'Полотно', price: 1700)
p.complectations.build(name: 'Комплект', price: 2235, description: "Комплект включает в себя:\nполотно - 1700,\nкоробка - 450,\nналичник полукруглый - 85.")
p.categories = Category.where(name: 'Ламинированные')
p.save

#Сезия ОП ИО
p = Product.new(name: 'Сезия', price: 1700, modification: 'итальянский орех ОП', description: 'Ламинированная остекленная дверь "Сезия" в цвете "итальянский орех".', basic_image: File.new(Rails.root.join('Doors images').join('Сезия Орех итальянский остекленная.jpg')), on_main_page: true, visible: true)
p.features << Feature.all
t = f[1].text_values.create(text: 'финиш-пленка производства Германии')
p.text_values << t
t = f[2].text_values.create(text: 'брус дерева хвойных пород')
p.text_values << t
t = f[3].text_values.create(text: 'МДФ ламинированный')
p.text_values << t
t = f[4].pictures.create(image: File.new(Rails.root.join('Doors images').join('итальянский орех.jpg')))
p.pictures << t
t = f[5].text_values.create(text: 'сотовое')
p.text_values << t
t = f[6].text_values.create(text: 'ламинированный МДФ')
p.text_values << t
t = f[7].text_values.create(text: 'белое матированное')
p.text_values << t
t = f[8].text_values.create(text: '600 × 2000')
p.text_values << t
t = f[8].text_values.create(text: '700 × 2000')
p.text_values << t
t = f[8].text_values.create(text: '800 × 2000')
p.text_values << t
t = f[8].text_values.create(text: '900 × 2000')
p.text_values << t
t = f[9].text_values.create(text: '38мм')
p.text_values << t
t = f[10].text_values.create(text: 'короб прямой 30 × 70 × 2100 мм')
p.text_values << t
t = f[11].text_values.create(text: 'наличник полукруглый 10 × 70 × 2150 мм.')
p.text_values << t
t = f[12].text_values.create(text: 'полиэтилен, пенопласт')
p.text_values << t

p.complectations.build(name: 'Полотно', price: 1700)
p.complectations.build(name: 'Комплект', price: 2235, description: "Комплект включает в себя:\nполотно - 1700,\nкоробка - 450,\nналичник полукруглый - 85.")
p.categories = Category.where(name: 'Ламинированные')
p.save


#Сезия ОП с фьюзингом ИО
p = Product.new(name: 'Сезия', price: 1900, modification: 'итальянский орех ОП с фьюзингом', description: 'Ламинированная дверь "Сезия" в цвете "итальянский орех" остекленная с фьюзингом.', basic_image: File.new(Rails.root.join('Doors images').join('Сезия Орех итальянский остекленная с фьюзингом.jpg')), on_main_page: true, visible: true)
p.features << Feature.all
t = f[1].text_values.create(text: 'финиш-пленка производства Германии')
p.text_values << t
t = f[2].text_values.create(text: 'брус дерева хвойных пород')
p.text_values << t
t = f[3].text_values.create(text: 'МДФ ламинированный')
p.text_values << t
t = f[4].pictures.create(image: File.new(Rails.root.join('Doors images').join('итальянский орех.jpg')))
p.pictures << t
t = f[5].text_values.create(text: 'сотовое')
p.text_values << t
t = f[6].text_values.create(text: 'ламинированный МДФ')
p.text_values << t
t = f[7].text_values.create(text: 'с фьюзингом')
p.text_values << t
t = f[8].text_values.create(text: '600 × 2000')
p.text_values << t
t = f[8].text_values.create(text: '700 × 2000')
p.text_values << t
t = f[8].text_values.create(text: '800 × 2000')
p.text_values << t
t = f[8].text_values.create(text: '900 × 2000')
p.text_values << t
t = f[9].text_values.create(text: '38мм')
p.text_values << t
t = f[10].text_values.create(text: 'короб прямой 30 × 70 × 2100 мм')
p.text_values << t
t = f[11].text_values.create(text: 'наличник полукруглый 10 × 70 × 2150 мм.')
p.text_values << t
t = f[12].text_values.create(text: 'полиэтилен, пенопласт')
p.text_values << t

p.complectations.build(name: 'Полотно', price: 1900)
p.complectations.build(name: 'Комплект', price: 2435, description: "Комплект включает в себя:\nполотно - 1900,\nкоробка - 450,\nналичник полукруглый - 85.")
p.categories = Category.where(name: 'Ламинированные')
p.save

#Сезия ГП
p = Product.new(name: 'Сезия', price: 1700, modification: 'миланский орех ГП', description: 'Ламинированная глухая дверь "Сезия" в цвете "миланский орех".', basic_image: File.new(Rails.root.join('Doors images').join('Сезия Орех миланский глухая.jpg')), on_main_page: true, visible: true)
p.features << Feature.all
t = f[1].text_values.create(text: 'финиш-пленка производства Германии')
p.text_values << t
t = f[2].text_values.create(text: 'брус дерева хвойных пород')
p.text_values << t
t = f[3].text_values.create(text: 'МДФ ламинированный')
p.text_values << t
t = f[4].pictures.create(image: File.new(Rails.root.join('Doors images').join('миланский орех.jpg')))
p.pictures << t
t = f[5].text_values.create(text: 'сотовое')
p.text_values << t
t = f[6].text_values.create(text: 'ламинированный МДФ')
p.text_values << t
t = f[7].text_values.create(text: 'нет (глухое)')
p.text_values << t
t = f[8].text_values.create(text: '600 × 2000')
p.text_values << t
t = f[8].text_values.create(text: '700 × 2000')
p.text_values << t
t = f[8].text_values.create(text: '800 × 2000')
p.text_values << t
t = f[8].text_values.create(text: '900 × 2000')
p.text_values << t
t = f[9].text_values.create(text: '38мм')
p.text_values << t
t = f[10].text_values.create(text: 'короб прямой 30 × 70 × 2100 мм')
p.text_values << t
t = f[11].text_values.create(text: 'наличник полукруглый 10 × 70 × 2150 мм.')
p.text_values << t
t = f[12].text_values.create(text: 'полиэтилен, пенопласт')
p.text_values << t

p.complectations.build(name: 'Полотно', price: 1700)
p.complectations.build(name: 'Комплект', price: 2235, description: "Комплект включает в себя:\nполотно - 1700,\nкоробка - 450,\nналичник полукруглый - 85.")
p.categories = Category.where(name: 'Ламинированные')
p.save

#Сезия ОП ИО
p = Product.new(name: 'Сезия', price: 1700, modification: 'миланский орех ОП', description: 'Ламинированная остекленная дверь "Сезия" в цвете "миланский орех".', basic_image: File.new(Rails.root.join('Doors images').join('Сезия Орех миланский остекленная.jpg')), on_main_page: true, visible: true)
p.features << Feature.all
t = f[1].text_values.create(text: 'финиш-пленка производства Германии')
p.text_values << t
t = f[2].text_values.create(text: 'брус дерева хвойных пород')
p.text_values << t
t = f[3].text_values.create(text: 'МДФ ламинированный')
p.text_values << t
t = f[4].pictures.create(image: File.new(Rails.root.join('Doors images').join('миланский орех.jpg')))
p.pictures << t
t = f[5].text_values.create(text: 'сотовое')
p.text_values << t
t = f[6].text_values.create(text: 'ламинированный МДФ')
p.text_values << t
t = f[7].text_values.create(text: 'белое матированное')
p.text_values << t
t = f[8].text_values.create(text: '600 × 2000')
p.text_values << t
t = f[8].text_values.create(text: '700 × 2000')
p.text_values << t
t = f[8].text_values.create(text: '800 × 2000')
p.text_values << t
t = f[8].text_values.create(text: '900 × 2000')
p.text_values << t
t = f[9].text_values.create(text: '38мм')
p.text_values << t
t = f[10].text_values.create(text: 'короб прямой 30 × 70 × 2100 мм')
p.text_values << t
t = f[11].text_values.create(text: 'наличник полукруглый 10 × 70 × 2150 мм.')
p.text_values << t
t = f[12].text_values.create(text: 'полиэтилен, пенопласт')
p.text_values << t

p.complectations.build(name: 'Полотно', price: 1700)
p.complectations.build(name: 'Комплект', price: 2235, description: "Комплект включает в себя:\nполотно - 1700,\nкоробка - 450,\nналичник полукруглый - 85.")
p.categories = Category.where(name: 'Ламинированные')
p.save


#Сезия ОП с фьюзингом ИО
p = Product.new(name: 'Сезия', price: 1700, modification: 'миланский орех ОП с фьюзингом', description: 'Ламинированная дверь "Сезия" в цвете "миланский орех" остекленная с фьюзингом.', basic_image: File.new(Rails.root.join('Doors images').join('Сезия Остекленое Милан орех фьюзинг.jpg')), on_main_page: true, visible: true)
p.features << Feature.all
t = f[1].text_values.create(text: 'финиш-пленка производства Германии')
p.text_values << t
t = f[2].text_values.create(text: 'брус дерева хвойных пород')
p.text_values << t
t = f[3].text_values.create(text: 'МДФ ламинированный')
p.text_values << t
t = f[4].pictures.create(image: File.new(Rails.root.join('Doors images').join('миланский орех.jpg')))
p.pictures << t
t = f[5].text_values.create(text: 'сотовое')
p.text_values << t
t = f[6].text_values.create(text: 'ламинированный МДФ')
p.text_values << t
t = f[7].text_values.create(text: 'с фьюзингом')
p.text_values << t
t = f[8].text_values.create(text: '600 × 2000')
p.text_values << t
t = f[8].text_values.create(text: '700 × 2000')
p.text_values << t
t = f[8].text_values.create(text: '800 × 2000')
p.text_values << t
t = f[8].text_values.create(text: '900 × 2000')
p.text_values << t
t = f[9].text_values.create(text: '38мм')
p.text_values << t
t = f[10].text_values.create(text: 'короб прямой 30 × 70 × 2100 мм')
p.text_values << t
t = f[11].text_values.create(text: 'наличник полукруглый 10 × 70 × 2150 мм.')
p.text_values << t
t = f[12].text_values.create(text: 'полиэтилен, пенопласт')
p.text_values << t

p.complectations.build(name: 'Полотно', price: 1700)
p.complectations.build(name: 'Комплект', price: 2235, description: "Комплект включает в себя:\nполотно - 1700,\nкоробка - 450,\nналичник полукруглый - 85.")
p.categories = Category.where(name: 'Ламинированные')
p.save



#Домино 
p = Product.new(name: 'Домино', price: 5810, modification: 'ПГ Венге', description: 'Модель "Домино" в цвете венге. Глухое полотно', basic_image: File.new(Rails.root.join('Doors images').join('Домино Глухая Венге.jpg')), on_main_page: true, visible: true)
p.features << Feature.all
p.features.delete(Feature.find_by(name: 'Багет'))
t = f[1].text_values.create(text: 'искусственный шпон')
p.text_values << t
t = f[2].text_values.create(text: 'МДФ и брус деревьев хвойных пород')
p.text_values << t
t = f[3].text_values.create(text: 'крепление без гвоздей (телескопическая система)')
p.text_values << t
t = f[4].pictures.create(image: File.new(Rails.root.join('Doors images').join('венге.jpg')))
p.pictures << t
t = f[5].text_values.create(text: 'сотовое')
p.text_values << t
t = f[7].text_values.create(text: 'нет (глухое)')
p.text_values << t
t = f[8].text_values.create(text: '600 × 2000')
p.text_values << t
t = f[8].text_values.create(text: '700 × 2000')
p.text_values << t
t = f[8].text_values.create(text: '800 × 2000')
p.text_values << t
t = f[8].text_values.create(text: '900 × 2000')
p.text_values << t
t = f[9].text_values.create(text: '40мм')
p.text_values << t
t = f[10].text_values.create(text: 'короб прямой 30 × 70 × 2100 мм')
p.text_values << t
t = f[11].text_values.create(text: 'Г-образный наличник прямой 20 x 70 x 2170 мм')
p.text_values << t
t = f[12].text_values.create(text: 'пенопласт, картон, полиэтилен')
p.text_values << t

p.complectations.build(name: 'Полотно', price: 5810)
p.complectations.build(name: 'Комплект', price: 7015, description: "Комплект включает:\nполотно - 5810 руб., \nкоробка (к-т) - 1025 руб., \nналичник телескопический – 180 руб.")
p.categories = Category.where(name: 'Экошпон')
p.save



p = Product.new(name: 'Домино', price: 5810, modification: 'ПО Венге', description: 'Модель "Домино" в цвете венге. Остекленное полотно', basic_image: File.new(Rails.root.join('Doors images').join('Домино Остекленная Венге.png')), on_main_page: true, visible: true)
p.features << Feature.all
p.features.delete(Feature.find_by(name: 'Багет'))
t = f[1].text_values.create(text: 'искусственный шпон')
p.text_values << t
t = f[2].text_values.create(text: 'МДФ и брус деревьев хвойных пород')
p.text_values << t
t = f[3].text_values.create(text: 'крепление без гвоздей (телескопическая система)')
p.text_values << t
t = f[4].pictures.create(image: File.new(Rails.root.join('Doors images').join('венге.jpg')))
p.pictures << t
t = f[5].text_values.create(text: 'сотовое')
p.text_values << t
t = f[7].text_values.create(text: 'белое матированное')
p.text_values << t
t = f[8].text_values.create(text: '600 × 2000')
p.text_values << t
t = f[8].text_values.create(text: '700 × 2000')
p.text_values << t
t = f[8].text_values.create(text: '800 × 2000')
p.text_values << t
t = f[8].text_values.create(text: '900 × 2000')
p.text_values << t
t = f[9].text_values.create(text: '40мм')
p.text_values << t
t = f[10].text_values.create(text: 'короб прямой 30 × 70 × 2100 мм')
p.text_values << t
t = f[11].text_values.create(text: 'Г-образный наличник прямой 20 x 70 x 2170 мм')
p.text_values << t
t = f[12].text_values.create(text: 'пенопласт, картон, полиэтилен')
p.text_values << t

p.complectations.build(name: 'Полотно', price: 5810)
p.complectations.build(name: 'Комплект', price: 7015, description: "Комплект включает:\nполотно - 5810 руб., \nкоробка (к-т) - 1025 руб., \nналичник телескопический – 180 руб.")
p.categories = Category.where(name: 'Экошпон')
p.save




p = Product.new(name: 'Домино', price: 5810, modification: 'ПГ Темный орех', description: 'Модель "Домино" в цвете "темный орех". Глухое полотно', basic_image: File.new(Rails.root.join('Doors images').join('Домино Глухая Темный орех.jpg')), on_main_page: true, visible: true)
p.features << Feature.all
p.features.delete(Feature.find_by(name: 'Багет'))
t = f[1].text_values.create(text: 'искусственный шпон')
p.text_values << t
t = f[2].text_values.create(text: 'МДФ и брус деревьев хвойных пород')
p.text_values << t
t = f[3].text_values.create(text: 'крепление без гвоздей (телескопическая система)')
p.text_values << t
t = f[4].pictures.create(image: File.new(Rails.root.join('Doors images').join('темный орех.jpg')))
p.pictures << t
t = f[5].text_values.create(text: 'сотовое')
p.text_values << t
t = f[7].text_values.create(text: 'нет (глухое)')
p.text_values << t
t = f[8].text_values.create(text: '600 × 2000')
p.text_values << t
t = f[8].text_values.create(text: '700 × 2000')
p.text_values << t
t = f[8].text_values.create(text: '800 × 2000')
p.text_values << t
t = f[8].text_values.create(text: '900 × 2000')
p.text_values << t
t = f[9].text_values.create(text: '40мм')
p.text_values << t
t = f[10].text_values.create(text: 'короб прямой 30 × 70 × 2100 мм')
p.text_values << t
t = f[11].text_values.create(text: 'Г-образный наличник прямой 20 x 70 x 2170 мм')
p.text_values << t
t = f[12].text_values.create(text: 'пенопласт, картон, полиэтилен')
p.text_values << t

p.complectations.build(name: 'Полотно', price: 5810)
p.complectations.build(name: 'Комплект', price: 7015, description: "Комплект включает:\nполотно - 5810 руб., \nкоробка (к-т) - 1025 руб., \nналичник телескопический – 180 руб.")
p.categories = Category.where(name: 'Экошпон')
p.save



p = Product.new(name: 'Домино', price: 5810, modification: 'ПО Темный орех', description: 'Межкомнатная дверь "Домино" серии "Принцип" в цвете "темный орех". Остекленное полотно. ', basic_image: File.new(Rails.root.join('Doors images').join('Домино Остекленная Темный орех.jpg')), on_main_page: true, visible: true)
p.features << Feature.all
p.features.delete(Feature.find_by(name: 'Багет'))
t = f[1].text_values.create(text: 'искусственный шпон')
p.text_values << t
t = f[2].text_values.create(text: 'МДФ и брус деревьев хвойных пород')
p.text_values << t
t = f[3].text_values.create(text: 'крепление без гвоздей (телескопическая система)')
p.text_values << t
t = f[4].pictures.create(image: File.new(Rails.root.join('Doors images').join('темный орех.jpg')))
p.pictures << t
t = f[5].text_values.create(text: 'сотовое')
p.text_values << t
t = f[7].text_values.create(text: 'белое матированное')
p.text_values << t
t = f[8].text_values.create(text: '600 × 2000')
p.text_values << t
t = f[8].text_values.create(text: '700 × 2000')
p.text_values << t
t = f[8].text_values.create(text: '800 × 2000')
p.text_values << t
t = f[8].text_values.create(text: '900 × 2000')
p.text_values << t
t = f[9].text_values.create(text: '40мм')
p.text_values << t
t = f[10].text_values.create(text: 'короб прямой 30 × 70 × 2100 мм')
p.text_values << t
t = f[11].text_values.create(text: 'Г-образный наличник прямой 20 x 70 x 2170 мм')
p.text_values << t
t = f[12].text_values.create(text: 'пенопласт, картон, полиэтилен')
p.text_values << t

p.complectations.build(name: 'Полотно', price: 5810)
p.complectations.build(name: 'Комплект', price: 7015, description: "Комплект включает:\nполотно - 5810 руб., \nкоробка (к-т) - 1025 руб., \nналичник телескопический – 180 руб.")
p.categories = Category.where(name: 'Экошпон')
p.save

=end