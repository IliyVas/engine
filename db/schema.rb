# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140925081228) do

  create_table "additional_pictures", force: true do |t|
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.integer  "product_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "additional_pictures", ["product_id"], name: "index_additional_pictures_on_product_id"

  create_table "carts", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "catalog_pages", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "categories", force: true do |t|
    t.string   "name"
    t.integer  "prev_id"
    t.boolean  "last",               default: true
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "product_prefix"
    t.string   "title"
    t.text     "description"
    t.integer  "order"
    t.boolean  "special",            default: false
    t.boolean  "hidden",             default: false
    t.string   "label_file_name"
    t.string   "label_content_type"
    t.integer  "label_file_size"
    t.datetime "label_updated_at"
    t.string   "slug"
    t.string   "keywords"
    t.text     "meta_description"
  end

  add_index "categories", ["slug"], name: "index_categories_on_slug", unique: true

  create_table "categories_features", force: true do |t|
    t.integer "category_id"
    t.integer "feature_id"
    t.integer "ordering"
  end

  add_index "categories_features", ["category_id"], name: "index_categories_features_on_category_id"
  add_index "categories_features", ["feature_id"], name: "index_categories_features_on_feature_id"

  create_table "categories_products", id: false, force: true do |t|
    t.integer "category_id", null: false
    t.integer "product_id",  null: false
  end

  create_table "category_templates", force: true do |t|
    t.integer  "category_id"
    t.integer  "features_template_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "category_templates", ["category_id"], name: "index_category_templates_on_category_id"
  add_index "category_templates", ["features_template_id"], name: "index_category_templates_on_features_template_id"

  create_table "cf_relationships", force: true do |t|
    t.integer  "category_id"
    t.integer  "feature_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "chosen_products", force: true do |t|
    t.integer  "cart_id"
    t.integer  "order_id"
    t.integer  "product_id"
    t.integer  "current_price"
    t.integer  "quantity",         default: 1
    t.integer  "complectation_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "chosen_values", force: true do |t|
    t.integer  "chosen_product_id"
    t.integer  "text_value_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "ckeditor_assets", force: true do |t|
    t.string   "data_file_name",               null: false
    t.string   "data_content_type"
    t.integer  "data_file_size"
    t.integer  "assetable_id"
    t.string   "assetable_type",    limit: 30
    t.string   "type",              limit: 30
    t.integer  "width"
    t.integer  "height"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "ckeditor_assets", ["assetable_type", "assetable_id"], name: "idx_ckeditor_assetable"
  add_index "ckeditor_assets", ["assetable_type", "type", "assetable_id"], name: "idx_ckeditor_assetable_type"

  create_table "colors", force: true do |t|
    t.string   "name"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "complectations", force: true do |t|
    t.string   "name"
    t.integer  "price"
    t.integer  "new_price"
    t.text     "description"
    t.integer  "product_id"
    t.boolean  "standart",    default: false
    t.integer  "order"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "content_pictures", force: true do |t|
    t.string   "content_file_name"
    t.string   "content_content_type"
    t.integer  "content_file_size"
    t.datetime "content_updated_at"
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "emails", force: true do |t|
    t.string   "email"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "features", force: true do |t|
    t.string   "name"
    t.boolean  "basic",         default: false
    t.boolean  "searchable",    default: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "use_as_filter"
    t.integer  "order"
  end

  create_table "features_products", force: true do |t|
    t.integer "feature_id"
    t.integer "product_id"
    t.integer "ordering"
  end

  add_index "features_products", ["feature_id"], name: "index_features_products_on_feature_id"
  add_index "features_products", ["product_id"], name: "index_features_products_on_product_id"

  create_table "features_templates", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "friendly_id_slugs", force: true do |t|
    t.string   "slug",                      null: false
    t.integer  "sluggable_id",              null: false
    t.string   "sluggable_type", limit: 50
    t.string   "scope"
    t.datetime "created_at"
  end

  add_index "friendly_id_slugs", ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true
  add_index "friendly_id_slugs", ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type"
  add_index "friendly_id_slugs", ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id"
  add_index "friendly_id_slugs", ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type"

  create_table "general_contents", force: true do |t|
    t.string   "name"
    t.text     "content"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "info_pages", force: true do |t|
    t.string   "title"
    t.text     "content"
    t.string   "subcategory"
    t.string   "info_category"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "page_description"
    t.string   "keywords"
    t.integer  "order"
    t.string   "slug"
  end

  add_index "info_pages", ["slug"], name: "index_info_pages_on_slug", unique: true

  create_table "mercury_images", force: true do |t|
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "modifications", force: true do |t|
    t.integer "product_id"
    t.integer "modification_id"
  end

  add_index "modifications", ["modification_id"], name: "index_modifications_on_modification_id"
  add_index "modifications", ["product_id"], name: "index_modifications_on_product_id"

  create_table "news_items", force: true do |t|
    t.string   "headline"
    t.string   "description"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.text     "content"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "orders", force: true do |t|
    t.string  "first"
    t.string  "second"
    t.string  "email"
    t.string  "phone"
    t.string  "payment_method"
    t.integer "sum"
    t.string  "payment_status"
    t.string  "status"
    t.text    "comment"
    t.boolean "delivery",       default: false
    t.text    "address"
  end

  create_table "page_contents", force: true do |t|
    t.string   "page"
    t.string   "content_type"
    t.text     "content"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "pictures", force: true do |t|
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.string   "image_fingerprint"
    t.integer  "text_value_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "pictures", ["text_value_id"], name: "index_pictures_on_text_value_id"

  create_table "products", force: true do |t|
    t.integer  "category_id"
    t.string   "name"
    t.integer  "price"
    t.integer  "new_price"
    t.string   "modification"
    t.text     "description"
    t.text     "short_description"
    t.string   "basic_image_file_name"
    t.string   "basic_image_content_type"
    t.integer  "basic_image_file_size"
    t.datetime "basic_image_updated_at"
    t.boolean  "visible",                  default: true
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "on_main_page",             default: false
    t.string   "slug"
    t.string   "full_name"
    t.string   "keywords"
    t.string   "vendor"
  end

  add_index "products", ["slug"], name: "index_products_on_slug", unique: true

  create_table "products_in_orders", force: true do |t|
    t.integer  "order_id"
    t.integer  "product_id"
    t.integer  "count",      default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "products_values", force: true do |t|
    t.integer "product_id"
    t.integer "text_value_id"
    t.integer "ordering"
  end

  add_index "products_values", ["product_id"], name: "index_products_values_on_product_id"
  add_index "products_values", ["text_value_id"], name: "index_products_values_on_text_value_id"

  create_table "related_categories", force: true do |t|
    t.integer "product_id"
    t.integer "related_category_id"
  end

  add_index "related_categories", ["product_id"], name: "index_related_categories_on_product_id"
  add_index "related_categories", ["related_category_id"], name: "index_related_categories_on_related_category_id"

  create_table "related_products", force: true do |t|
    t.integer "product_id"
    t.integer "related_product_id"
  end

  add_index "related_products", ["product_id"], name: "index_related_products_on_product_id"
  add_index "related_products", ["related_product_id"], name: "index_related_products_on_related_product_id"

  create_table "simple_slides", force: true do |t|
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.string   "link"
    t.integer  "order"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "site_options", force: true do |t|
    t.string   "name"
    t.string   "value"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "slide_items", force: true do |t|
    t.string   "item_type"
    t.text     "data"
    t.text     "style"
    t.integer  "slide_id"
    t.text     "content"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "slide_items", ["slide_id"], name: "index_slide_items_on_slide_id"

  create_table "sliders", force: true do |t|
    t.string   "option"
    t.string   "value"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "slides", force: true do |t|
    t.text     "data"
    t.text     "style"
    t.integer  "order"
    t.integer  "pattern"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "template_features", force: true do |t|
    t.integer  "features_template_id"
    t.integer  "feature_id"
    t.integer  "ordering"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "template_features", ["feature_id"], name: "index_template_features_on_feature_id"
  add_index "template_features", ["features_template_id"], name: "index_template_features_on_features_template_id"

  create_table "template_values", force: true do |t|
    t.integer  "template_feature_id"
    t.integer  "text_value_id"
    t.integer  "ordering"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "template_values", ["template_feature_id"], name: "index_template_values_on_template_feature_id"
  add_index "template_values", ["text_value_id"], name: "index_template_values_on_text_value_id"

  create_table "text_values", force: true do |t|
    t.text     "text"
    t.integer  "feature_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "text_values", ["feature_id"], name: "index_text_values_on_feature_id"

  create_table "users", force: true do |t|
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "admin",                  default: false
    t.integer  "failed_attempts"
    t.string   "unlock_token"
    t.datetime "locked_at"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true

end
