class CreateTextValues < ActiveRecord::Migration
  def change
    create_table :text_values do |t|
      t.text :text
      t.references :feature, index: true

      t.timestamps
    end
  end
end
