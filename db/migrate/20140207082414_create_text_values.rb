class CreateTextValues < ActiveRecord::Migration
  def change
    create_table :text_values do |t|
      t.text :text
      t.integer :product_id
      t.integer :feature_id
      
      t.timestamps
    end
  end
end
