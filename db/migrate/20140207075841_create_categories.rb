class CreateCategories < ActiveRecord::Migration
  def change
    create_table :categories do |t|
      t.string :name
      t.integer :prev_id
      t.boolean :last, :default => true

      t.timestamps
    end
  end
end
