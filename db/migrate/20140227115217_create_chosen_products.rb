class CreateChosenProducts < ActiveRecord::Migration
  def change
    create_table :chosen_products do |t|
      t.references :cart
      t.references :order
      t.references :product
      t.integer :current_price
      t.integer :quantity, default: 1
      t.references :complectation

      t.timestamps
    end
  end
end
