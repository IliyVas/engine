class CreateSiteOptions < ActiveRecord::Migration
  def change
    create_table :site_options do |t|
      t.string :name
      t.string :value

      t.timestamps
    end
  end
end
