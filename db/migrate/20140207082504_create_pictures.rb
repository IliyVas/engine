class CreatePictures < ActiveRecord::Migration
  def change
    create_table :pictures do |t|
      t.attachment :image
      t.integer :product_id
      t.integer :feature_id

      t.timestamps
    end
  end
end
