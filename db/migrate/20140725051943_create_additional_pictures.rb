class CreateAdditionalPictures < ActiveRecord::Migration
  def change
    create_table :additional_pictures do |t|
      t.attachment :image
      t.references :product, index: true

      t.timestamps
    end
  end
end
