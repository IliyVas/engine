class CreateTemplateValues < ActiveRecord::Migration
  def change
    create_table :template_values do |t|
      t.references :template_feature, index: true
      t.references :text_value, index: true
      t.integer :ordering

      t.timestamps
    end
  end
end
