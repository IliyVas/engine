class CreateFeaturesTemplates < ActiveRecord::Migration
  def change
    create_table :features_templates do |t|
      t.string :name

      t.timestamps
    end
  end
end
