class CreateModifications < ActiveRecord::Migration
  def change
    create_table :modifications do |t|
      t.references :product, index: true
      t.references :modification, index: true
    end
  end
end
