class CreateProductsValues < ActiveRecord::Migration
  def change
    create_table :products_values do |t|
      t.references :product, index: true
      t.references :text_value, index: true
      t.integer :ordering
    end
  end
end
