class CreateProductsInOrders < ActiveRecord::Migration
  def change
    create_table :products_in_orders do |t|
      t.integer :order_id
      t.integer :product_id
      t.integer :count, :default => 0

      t.timestamps
    end
  end
end
