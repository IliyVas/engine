class CreateNewsItems < ActiveRecord::Migration
  def change
    create_table :news_items do |t|
      t.string :headline
      t.string :description
      t.attachment :image
      t.text :content

      t.timestamps
    end
  end
end
