class CreateFeaturesProducts < ActiveRecord::Migration
  def change
    create_table :features_products do |t|
      t.references :feature, index: true
      t.references :product, index: true
      t.integer :ordering
    end
  end
end
