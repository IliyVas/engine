class AddSlugToInfoPage < ActiveRecord::Migration
  def change
    add_column :info_pages, :slug, :string
    add_index :info_pages, :slug, :unique => true
    InfoPage.find_each(&:save)
  end
end
