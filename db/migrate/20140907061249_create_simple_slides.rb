class CreateSimpleSlides < ActiveRecord::Migration
  def change
    create_table :simple_slides do |t|
      t.attachment :image
      t.string :link
      t.integer :order

      t.timestamps
    end
  end
end
