class CreateProductsInCarts < ActiveRecord::Migration
  def change
    create_table :products_in_carts do |t|
      t.integer :cart_id
      t.integer :order_id
      t.integer :product_id
      t.integer :quantity

      t.timestamps
    end
  end
end
