class AddUseAsFilterToFeatures < ActiveRecord::Migration
  def change
  	add_column :features, :use_as_filter, :boolean
  end
end
