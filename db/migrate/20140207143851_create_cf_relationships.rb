class CreateCfRelationships < ActiveRecord::Migration
  def change
    create_table :cf_relationships do |t|
      t.integer :category_id
      t.integer :feature_id

      t.timestamps
    end
  end
end
