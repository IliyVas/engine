class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.integer :category_id
      t.string :name
      t.integer :price
      t.integer :new_price
      t.string :modification
      t.text :description
      t.text :short_description
      t.attachment :basic_image
      t.boolean :visible , :default => true
      

      t.timestamps
    end
  end
end
