class CreateCategoriesFeatures < ActiveRecord::Migration
  def change
    create_table :categories_features do |t|
      t.references :category, index: true
      t.references :feature, index: true
      t.integer :ordering
    end
  end
end
