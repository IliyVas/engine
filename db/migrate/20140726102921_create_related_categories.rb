class CreateRelatedCategories < ActiveRecord::Migration
  def change
    create_table :related_categories do |t|
      t.references :product, index: true
      t.references :related_category, index: true
    end
  end
end
