# -*- encoding : utf-8 -*-
class AddVendorToProduct < ActiveRecord::Migration
  def change
    add_column :products, :vendor, :string
    Product.find_each do |product|
    	product.vendor = "Производитель"
    	product.save
    end
  end
end
