class AddMetaToInfoPages < ActiveRecord::Migration
  def change
    add_column :info_pages, :page_description, :string
    add_column :info_pages, :keywords, :string
    add_column :info_pages, :order, :integer
  end
end
