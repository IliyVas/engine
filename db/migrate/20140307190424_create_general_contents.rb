class CreateGeneralContents < ActiveRecord::Migration
  def change
    create_table :general_contents do |t|
      t.string :name
      t.text :content

      t.timestamps
    end
  end
end
