class CreateSlides < ActiveRecord::Migration
  def change
    create_table :slides do |t|
      t.text :data
      t.text :style
      t.integer :order
      t.integer :pattern

      t.timestamps
    end
  end
end
