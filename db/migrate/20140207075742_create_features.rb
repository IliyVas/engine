class CreateFeatures < ActiveRecord::Migration
  def change
    create_table :features do |t|
      t.string :name
      t.boolean :basic, :default => false
      t.boolean :searchable, :default => false

      t.timestamps
    end
  end
end
