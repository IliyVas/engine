class CreateCategoryTemplates < ActiveRecord::Migration
  def change
    create_table :category_templates do |t|
      t.references :category, index: true
      t.references :features_template, index: true

      t.timestamps
    end
  end
end
