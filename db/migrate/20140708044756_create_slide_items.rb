class CreateSlideItems < ActiveRecord::Migration
  def change
    create_table :slide_items do |t|
      t.string :item_type
      t.text :data
      t.text :style
      t.belongs_to :slide, index: true
      t.text :content
      t.attachment :image

      t.timestamps
    end
  end
end
