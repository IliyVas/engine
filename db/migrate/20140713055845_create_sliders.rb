class CreateSliders < ActiveRecord::Migration
  def change
    create_table :sliders do |t|
      t.string :option
      t.string :value
      t.timestamps
    end
  end
end
