class ChangePrefixColumnName < ActiveRecord::Migration
  def change
	rename_column :products, :prefix, :full_name
  end
end
