class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.string :first
      t.string :second
      t.string :email
      t.string :phone
      t.string :payment_method
      t.string :payment_status
      t.string :status
      t.text :comment
      t.boolean :delivery
    end
  end
end
