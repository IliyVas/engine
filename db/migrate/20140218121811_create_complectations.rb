class CreateComplectations < ActiveRecord::Migration
  def change
    create_table :complectations do |t|
      t.string :name
      t.integer :price
      t.integer :new_price
      t.text :description
      t.integer :product_id
      t.boolean :standart, :default => false
      t.integer :order

      t.timestamps
    end
  end
end
