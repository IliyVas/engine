class CreateInfoPages < ActiveRecord::Migration
  def change
    create_table :info_pages do |t|
      t.string :title
      t.text :content
      t.string :subcategory
      t.string :info_category

      t.timestamps
    end
  end
end
