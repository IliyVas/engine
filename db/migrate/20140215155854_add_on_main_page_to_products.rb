class AddOnMainPageToProducts < ActiveRecord::Migration
  def change
    add_column :products, :on_main_page, :boolean, :default => false
  end
end
