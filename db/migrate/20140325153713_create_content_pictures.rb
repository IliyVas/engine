class CreateContentPictures < ActiveRecord::Migration
  def change
    create_table :content_pictures do |t|
      t.attachment :content
      t.string :name
      t.timestamps
    end
  end
end
