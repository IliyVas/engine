class CreatePictures < ActiveRecord::Migration
  def change
    create_table :pictures do |t|
      t.attachment :image
      t.string :image_fingerprint
      t.references :text_value, index: true

      t.timestamps
    end
  end
end
