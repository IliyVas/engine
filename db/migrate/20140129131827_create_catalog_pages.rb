class CreateCatalogPages < ActiveRecord::Migration
  def change
    create_table :catalog_pages do |t|
      t.string :name

      t.timestamps
    end
  end
end
