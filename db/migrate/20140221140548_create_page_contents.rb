class CreatePageContents < ActiveRecord::Migration
  def change
    create_table :page_contents do |t|
      t.string :page
      t.string :content_type
      t.text :content

      t.timestamps
    end
  end
end
