class CreateChosenValues < ActiveRecord::Migration
  def change
    create_table :chosen_values do |t|
      t.references :chosen_product
      t.references :text_value

      t.timestamps
    end
  end
end
