class AddSpecialAttrToCategories < ActiveRecord::Migration
  def change
    add_column :categories, :special, :boolean, :default => false
    add_column :categories, :hidden, :boolean, :default => false
    add_attachment :categories, :label
  end
end
