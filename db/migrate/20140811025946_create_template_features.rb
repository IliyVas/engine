class CreateTemplateFeatures < ActiveRecord::Migration
  def change
    create_table :template_features do |t|
      t.references :features_template, index: true
      t.references :feature, index: true
      t.integer :ordering

      t.timestamps
    end
  end
end
