class AddSlugToProduct < ActiveRecord::Migration
  def change
    add_column :products, :slug, :string
    add_index :products, :slug, :unique => true
    Product.find_each(&:save)
  end
end
