# -*- encoding : utf-8 -*-
class Product::Presenter
	def initialize(product)
		@product = product
	end

	def product_pictures
		@product_pictures ||= @product.additional_pictures
	end

	def full_name
		(@product.category.try(:product_prefix) || '') +' '+@product.name
	end

	def basic_features_without_options
		@basic_features_without_options ||= one_child_features(true)
	end

	def additional_features_without_options
		@additional_features_without_options ||= one_child_features(false)
	end

	def features_with_options
		@checked = {}
		@features_with_options ||= @product.features
		.where.not(id: (additional_features_without_options.pluck('features.id') | basic_features_without_options.pluck('features.id')))
	end

	def checked?(value)
		@product.products_values.find_by(text_value_id: value.id).ordering == 1
	end

	def feature_values(feature)
		values = @product.text_values.where(feature_id: feature.id)

		case values.size
		when 0
			Array.new
		when 1
			@last = values.take
		else
			values
		end

	end

	def last
		@last
	end

	def info
		GeneralContent.find_by(name: 'product_page_info').try(:content).try(:html_safe)
	end

	def radio_name(feature)
		'chosen_product[chosen_values_attributes][' + feature.id.to_s + '][text_value_id]'
	end

	def similiar_products
		ids = @product.text_values.where(feature_id: basic_features_without_options.pluck(:id)).pluck(:id)
		category_products = Product.where('category_id = ? AND products.id <> ?', @product.category_id, @product.id)
		products = category_products.joins(:text_values).where(:text_values => {id: ids}).group('text_values.id')
		.having('count(text_values.id) = ?', ids.size)
		if products.empty?
			if category_products.empty?
				[@product]
			else
				if category_products.size > 4
					category_products.order('RANDOM()').first(4)
				else
					category_products
				end
			end			
		else
			if products.count.size > 4
				products.order('RANDOM()').first(4)
			else
				products
			end
		end
	end





=begin
			<%= render partial: 'product_features', locals: {basic: true, feature_type: 'basic', f: f} %>
			<% complectations = Complectation.where(product_id: @product.id) %>
			<div>
				<% unless complectations.empty? %>
					<div class="complectation-select selected">
						<%= f.fields_for :chosen_values, f.object.chosen_values.build do |comp_f| %>
							<%= comp_f.hidden_field :complectation_id, value: complectations.first.id %>
							<p>
								<%= comp_f.radio_button :complectation_id, complectations.first.id, checked: true %>
								<%= complectations.first.name %>
							</p>
							<p>Цена:</p>
								<p><%= complectations.shift.price %> руб.</p>
					</div>
							<% complectations.each do |complectation| %>
								<div class="complectation-select not-selected" alt="Выбрать">
									<%= comp_f.radio_button :complectation_id, complectation.id %>
									<p><%= complectation.name %></p>
									<p>Цена:</p>
									<p><%= complectation.price %> руб.</p>
								</div>
							<% end %>
						<% end %>
				<% end %>
			</div>
			<%= f.submit 'Добавить в корзину', id: @product.id, class: 'add-to-cart cart-submit' %>
			<div class="add-to-cart quantity-input">
				<%= f.number_field :quantity, value: 1, step: 1, min: 1 %>
			</div>
			<div class="add-to-cart quantity-ctrl">
				<a id="quantity_increase"> &#9650; </a>
				<a id="quantity_decrease"> &#9660; </a>
			</div>
			<%= f.hidden_field :product_id, value: @product.id %>
			<% complectations.each do |complectation| %>
				<div class="complectations-description">
					<p><%= complectation.description %></p>
				</div>
			<% end %>
			<%= render partial: 'product_features', locals: {basic: false, feature_type: 'sub', f: f} %>
		</div>
	</div>
</div>
<% end %>
<% content_for :divider do %>
	<div class="h-divider">
		<div class="row">
			<div class="full-name">
				<h3><%= (  <%= @product.modification %></h3>
			</div>
			<div class="full-path">
				<span>Вы находитесь здесь:</span>
				<span>
					<%= link_to 'Каталог', products_path %>
					<% @full_path.reverse_each do |cat| %>
						/ <%= link_to cat.name, products_path(:category_id => cat.id) %>
					<% end %>
				</span>
			</div>
		</div>
	</div>
<% end %>


<%= button_to('Удалить', product_path(@product), method: :delete, class: "btn btn-danger btn-sm") if current_user.try(:admin?)  %>

<%= button_to('Редактировать', edit_product_path(@product), method: :get, class: "btn btn-danger btn-sm") if current_user.try(:admin?)  %>
<%= form_for @product_in_cart, :url => {:controller => 'product_in_cart', :action => 'create'}, remote:true, method: :post do |f| %>
=end
  private

  	def one_child_features(basic)
  		@product.features.includes( :text_values=>:products)
  		.where('features.basic = ? AND products_values.product_id =?',basic, @product.id)
		.group('features.id').having('count(products_values.product_id) = 1')
	end
end

#@product.features.includes( :text_values=>:products).includes(:pictures => :products)
#.where('features.basic = ? AND (products_text_values.product_id =? OR pictures_products.product_id = ?)',basic, @product.id, @product.id)
#.group('features.id').having('count(products_text_values.product_id) = 1 OR count(pictures_products.product_id) = 1')
#.order(:order)
