class GeneralContentPresenter

	def initialize
	end

	def business_time
		GeneralContent.find_by(name: 'business_time').content
	end

	def phones
		GeneralContent.where(name: 'phone').pluck(:content)
	end

	def footer_left
		GeneralContent.find_by(name: 'footer_left').content
	end

	def footer_right
		GeneralContent.find_by(name: 'footer_right').content
	end

	def main_spec_cat_without_children
		Category.where(special: true, prev_id: nil, hidden: false).where.not(id: Category.pluck(:prev_id).uniq).order(:order)
	end

	def main_basic_cat_without_children
		Category.where(special: false, prev_id: nil, hidden: false).where.not(id: Category.pluck(:prev_id).uniq).order(:order)
	end

	def main_spec_cat_with_children
		Category.where(special: true, prev_id: nil, hidden:false, id: Category.pluck(:prev_id).uniq).order(:order)
	end

	def main_basic_cat_with_children
		Category.where(special: false, prev_id: nil, hidden: false, id: Category.pluck(:prev_id).uniq).order(:order)
	end
end