# -*- encoding : utf-8 -*-
class InfoPage::Presenter
	def initialize(info_category)
		@info_category = info_category
	end

	def info_category
		@info_category
	end
	
	def category_pages
		@category_pages ||= InfoPage.where(info_category: @info_category)
	end

	def pages_in_category
		@size ||= category_pages.size
	end

	def is_single_in_category?
		@single ||= pages_in_category == 1
	end

	def subcategories
		InfoPage.where(info_category: @info_category)
			.order(:order).select(:subcategory)
			.distinct.pluck(:subcategory)
	end

	def isContacts?
		@info_category == 'Контакты'
	end

	def pages_in_subcategory(sub)
		InfoPage.where(info_category: @info_category, subcategory: sub).order(:order)
	end

	def array_for_select(new)
		arr = category_pages.where.not(subcategory: nil, subcategory: '')
							.distinct.pluck(:subcategory)
		arr.push(new) if new
		arr
	end
end