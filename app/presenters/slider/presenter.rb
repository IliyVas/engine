class Slider::Presenter
	def initialize
	end

	def slide_transition_speed
		Slider.find_by(:option => 'slideTransitionSpeed').try(:value)		
	end

	def controls
		Slider.find_by(:option => 'controls').try(:value)		
	end

	def pager
		Slider.find_by(:option => 'pager').try(:value)		
	end

	def pause_on_hover
		Slider.find_by(:option => 'pauseOnHover').try(:value)		
	end

	def interval
		Slider.find_by(:option => 'interval').try(:value)
	end
end