# -*- encoding : utf-8 -*-
class Slide::Presenter 
	def initialize(slide)
		if slide.new_record?
			@action = :create
			@name = 'Новый слайд'
		else
			@action = :update
			@name = "Cлайд #{slide.order}"
		end
	end

	def name
		@name
	end

	def action
		@action
	end

	def data_in_out
		['fade', 'none', 'left', 'topLeft', 'bottomLeft', 'right', 'topRight', 'bottomRight', 'top', 'bottom']
	end

	def data_ease
		[	'easeInSine', 'easeOutSine', 'easeInOutSine', 
			'easeInQuad', 'easeOutQuad', 'easeInOutQuad', 
			'easeInCubic', 'easeOutCubic', 'easeInOutCubic', 
			'easeInQuart', 'easeOutQuart', 'easeInOutQuart', 
			'easeInQuint', 'easeOutQuint', 'easeInOutQuint', 
			'easeInExpo', 'easeOutExpo', 'easeInOutExpo', 
			'easeInCirc', 'easeOutCirc', 'easeInOutCirc', 
			'easeInBack', 'easeOutBack', 'easeInOutBack', 
			'easeInElastic', 'easeOutElastic', 'easeInOutElastic', 
			'easeInBounce', 'easeOutBounce', 'easeInOutBounce' ]
	end
end