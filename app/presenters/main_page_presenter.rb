class MainPagePresenter
	def initialize
	end

	def main_categories
		Category.where(prev_id: nil).order(:order)
	end

	def products(main_cat)
		Product.joins(:category).where(categories: {id: subcategories(main_cat), hidden: false, special: false},
			on_main_page: true, visible: true)
	end

	def content
		@content ||= GeneralContent.find_by(name: 'main_page_content').content
	end

	def title
		GeneralContent.find_by(name: 'main_page_title').content
	end

	def keywords
		GeneralContent.find_by(name: 'main_page_keywords').content
	end

	def description
		GeneralContent.find_by(name: 'main_page_description').content
	end

	private
		def subcategories(category, id_array = Array.new)
		    if category.last
		      id_array.push(category.id)
		    else
		      Category.where(prev_id: category.id).each { |cat| subcategories(cat, id_array)}
		      id_array
		    end
		end
end