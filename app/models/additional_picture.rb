class AdditionalPicture < ActiveRecord::Base
	has_attached_file :image, :styles => { :small => "150x150>" },
                  :url  => "/assets/additional_pictures/:id/:style/:basename.:extension",
                  :path => ":rails_root/public/assets/additional_pictures/:id/:style/:basename.:extension"

	belongs_to :product
end
