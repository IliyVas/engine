class SlideItem < ActiveRecord::Base
  belongs_to :slide
  has_attached_file :image,
            :url  => "/assets/feat_pictures/:id/:style/:basename.:extension",
            :path => ":rails_root/public/assets/feat_pictures/:id/:style/:basename.:extension"

  def get_content_object
  	@item_types = ['button', 'list']
  	@obj ||= Marshal::load(self.content) if @item_types.include?(self.item_type) 
  end

  def get_data(key)
  	if self.data
		@data ||= Marshal::load(self.data)
		key = key.to_sym 
		key == :position ? @data[key].split(',') : @data[key]
	end
  end
end
