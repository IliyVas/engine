 class Product < ActiveRecord::Base
    extend FriendlyId
    friendly_id :name, :use => :slugged
    
	has_attached_file :basic_image, :styles => { :small => "200x180>", :normal => "340x400" },
                  :url  => "/assets/products/:id/:style/:basename.:extension",
                  :path => ":rails_root/public/assets/products/:id/:style/:basename.:extension"

	has_and_belongs_to_many :categories
	belongs_to :category

	has_and_belongs_to_many :modifications, 
            class_name: "Product", 
            join_table: :modifications, 
            foreign_key: :product_id, 
            association_foreign_key: :modification_id

    has_and_belongs_to_many :related_products, 
            class_name: "Product", 
            join_table: :related_products, 
            foreign_key: :product_id, 
            association_foreign_key: :related_product_id

    has_and_belongs_to_many :related_categories,
    		class_name: "Category",
    		join_table: :related_categories,
    		foreign_key: :product_id,
    		association_foreign_key: :related_category_id

	has_many :text_values, :through => :products_values
	has_many :products_values, class_name: 'ProductsValues'

	has_many :additional_pictures
    accepts_nested_attributes_for :additional_pictures, allow_destroy: true


	has_many :complectations, dependent: :destroy
	accepts_nested_attributes_for :complectations, allow_destroy: true

	has_many :features, -> {order 'features_products.ordering ASC'}, :through => :features_products
	has_many :features_products, class_name: 'FeaturesProducts'

    self.per_page = 10

    def current_price
        new_price.nil? || new_price == 0 ? price : new_price
    end
end
