class Slide < ActiveRecord::Base
	has_many :slide_items, dependent: :destroy

	def get_data(key)
		puts 'aAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA' unless @data
		if self.data
			@data ||= Marshal::load(self.data)
			@data[key.to_sym]
		end
	end

	def set_data(data)
		self.data = Marshal::dump(data)		
	end
end
