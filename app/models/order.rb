# -*- encoding : utf-8 -*-
class Order < ActiveRecord::Base
	has_many :products, through: :chosen_products
	has_many :chosen_products
	accepts_nested_attributes_for :chosen_products

	validates :first, :second, :email, :phone,
       :payment_method, :chosen_products, presence: true

	def products_to_array
		products_array = [['Изображение', 'Наименование', 'Комплектность и характеристики', 'Цена', 'Количество', 'Сумма']]
		chosen_products.each do |chosen_product|
			product = chosen_product.product
			arr = [{:image => product.basic_image.path(:small), :fit => [110, 110]},
				product.name,
				(['Комплектность: ' + chosen_product.complectation.name] + 
				 chosen_product.text_values.map {|val| val.feature.name + ': ' + val.text}).join("\n"),
				chosen_product.current_price.to_s + "<font name='Rouble'>a</font>",
				chosen_product.quantity,
				(product.price * chosen_product.quantity).to_s + "<font name='Rouble'>a</font>"]
			products_array.push(arr)
		end
		products_array
	end

	def sum
		chosen_products.map{|c| c.quantity * c.current_price}.sum
	end
end
