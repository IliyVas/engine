class Cart < ActiveRecord::Base
	has_many :products, through: :chosen_products
	has_many :chosen_products

	def sum
		sum = 0
		chosen_products.each do |p|
			sum += p.quantity * p.current_price
		end
		sum
	end
end
