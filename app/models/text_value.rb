class TextValue < ActiveRecord::Base
  belongs_to :feature
  has_one :picture
  accepts_nested_attributes_for :picture, allow_destroy: true
  has_many :products, :through => :products_values
  has_many :products_values, class_name: 'ProductsValues', dependent: :destroy
end
