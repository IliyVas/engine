class ContentPicture < ActiveRecord::Base
	has_attached_file :content,
                  :url  => "/assets/content_pictures/:style/:basename.:extension",
                  :path => ":rails_root/public/assets/content_pictures/:style/:basename.:extension"
end
