class InfoPage < ActiveRecord::Base
	extend FriendlyId
	friendly_id :title, :use => :slugged

	def html_content
		self.content.html_safe
	end
	
	def content(replace_form = true)
		if replace_form
			read_attribute(:content).gsub('form', 'div')
		else
			read_attribute(:content)
		end
	end
end
