class SimpleSlide < ActiveRecord::Base
	has_attached_file :image, :styles => {:normal => "1260x300!", :preview => "630x150!"  },
                  :url  => "/assets/slides/:id/:style/:basename.:extension",
                  :path => ":rails_root/public/assets/slides/:id/:style/:basename.:extension"

end
