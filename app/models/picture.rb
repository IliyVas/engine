class Picture < ActiveRecord::Base
	has_attached_file :image, :styles => { :small => "100x100>"},
                  :url  => "/assets/products/:id/:style/:basename.:extension",
                  :path => ":rails_root/public/assets/products/:id/:style/:basename.:extension"

	belongs_to :text_value

	def small_url
		self.image.url(:small)		
	end
end
