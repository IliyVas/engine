class Feature < ActiveRecord::Base
	has_and_belongs_to_many :products
	#remove?
	has_many :categories, :through => :categories_features
	has_many :categories_features
	#remove?
	has_many :text_values
	accepts_nested_attributes_for :text_values, allow_destroy: true, reject_if: lambda {|attributes| attributes[:text].blank? && !attributes[:picture_attributes]}
end
