class GeneralContent < ActiveRecord::Base
	def self.update_content(hash)
		hash.each_pair do |key, value|
			if value.kind_of?(Array)
				self.where(name: key).each { |content| content.destroy }
				value.each { |val| self.create(name: key, content: val) }
			else
				object = self.find_by(name: key) || self.new(name: key)
				object.content = value
				object.save
			end
		end
	end
end
