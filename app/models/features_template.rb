class FeaturesTemplate < ActiveRecord::Base
	has_many :features, -> {order 'template_features.ordering ASC'}, through: :template_features
	has_many :template_features, -> {order 'ordering ASC'}
end
