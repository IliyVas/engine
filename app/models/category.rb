# -*- encoding : utf-8 -*-
class Category < ActiveRecord::Base
	extend FriendlyId
	friendly_id :name, :use => :slugged

	has_attached_file :label, :styles => { :small => "50x50>" },
                  :url  => "/assets/categories/:id/:style/:basename.:extension",
                  :path => ":rails_root/public/assets/categories/:id/:style/:basename.:extension"
	
	has_and_belongs_to_many :products

	has_many :features, -> {order 'categories_features.ordering ASC'}, :through => :categories_features
	has_many :categories_features, class_name: "CategoriesFeatures"
	accepts_nested_attributes_for :features

	belongs_to :category, foreign_key: "prev_id"
	has_many :categories, foreign_key: "prev_id"

	has_one :features_template, through: :category_template
	has_one  :category_template

	def self.get_lasts_parents
		lasts = where(last: true, special: false)
		lasts_parents = where(id: lasts.pluck(:prev_id)).to_a
		last_in_root = lasts.where(prev_id:nil)
		if last_in_root.exists?
			empty_category = Category.new(id: nil, name: 'Без категории')
			empty_category.categories = last_in_root
			lasts_parents.push empty_category
		end
		lasts_parents
	end

	def self.get_first_last_category_prefix
		category = Category.where.not(prev_id:nil).find_by( last: true, special: false )
		category.product_prefix ? category.product_prefix : ""
	end

	def self.special
		where(special: true)
	end

	def style
		style = Array.new
		style.push 'main' if !last || prev_id.nil?
		style.push 'special' if special
		style.join ' '
	end
end
