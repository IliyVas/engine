class SpecialCategory < ActiveRecord::Base
	has_many :products, through: :products_in_special_category
	has_many :products_in_special_category
end
