class ProductsInCart < ActiveRecord::Base
	has_many :text_values, through: :chosen_values
	has_many :pictures, through: :chosen_values
 	has_many :complectations, through: :chosen_values
 	has_many :chosen_values, dependent: :destroy
 	accepts_nested_attributes_for :chosen_values
 	belongs_to :cart
 	belongs_to :order
 	belongs_to :product
end
