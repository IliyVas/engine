class ChosenProduct < ActiveRecord::Base
	before_create :check_uniqueness

	has_many :text_values, through: :chosen_values
 	has_one :complectation, through: :chosen_product
 	has_one :chosen_product, :foreign_key => :id, :primary_key => :id
    belongs_to :complectation
 	has_many :chosen_values, dependent: :destroy
 	accepts_nested_attributes_for :chosen_values
 	belongs_to :cart
 	belongs_to :order
 	belongs_to :product

    validates :product, :quantity, :current_price, presence: true

	private
		def check_uniqueness
			ChosenProduct.where(
             cart_id: cart_id, 
             product_id: product_id, 
             current_price: current_price, 
             complectation_id: complectation_id).each do |chosen_product|

				if (chosen_product.chosen_values.pluck(:text_value_id)-
                   chosen_values.map(&:text_value_id)).empty?
				    
                    logger.debug quantity
                    new_quantity = quantity + chosen_product.quantity
                    logger.debug new_quantity
				    chosen_product.update_attributes quantity: new_quantity
				    return false
                end
            end
            return true
		end
end
