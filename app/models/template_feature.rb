class TemplateFeature < ActiveRecord::Base
  belongs_to :features_template
  belongs_to :feature
  has_many :text_values, through: :template_values
  has_many :template_values
end
