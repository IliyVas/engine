class PageContent < ActiveRecord::Base
	validates :content, presence: true
end
