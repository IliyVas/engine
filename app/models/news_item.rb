class NewsItem < ActiveRecord::Base
	has_attached_file :image, :styles => { :medium => "300x300>", :small => "200x200" },
                  :url  => "/assets/news/:id/:style/:basename.:extension",
                  :path => ":rails_root/public/assets/news/:id/:style/:basename.:extension"
end
