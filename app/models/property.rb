class Property < ActiveRecord::Base
	belongs_to :table_attr
	has_many :property_values, :dependent => :destroy
	accepts_nested_attributes_for :property_values, :allow_destroy => true
	has_and_belongs_to_many :tables
end
