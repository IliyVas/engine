class Complectation < ActiveRecord::Base
	belongs_to :product

	def current_price
		new_price.nil? || new_price == 0 ? price : new_price
	end
end
