class TablesController < ApplicationController
before_action :check_user_role
  def new
  	@table = Table.new
  	if params[:attr_num].nil?
  		@table.table_attrs.build(attr_type:'Название')
  		@table.table_attrs.build(attr_type:'Цена')
  		@table.table_attrs.build(attr_type:'Количество', visible: '1')
  	else
  		params[:attr_num].to_i.times { @table.table_attrs.build }
  	end
  end
  
  def create
  	@table = Table.new(table_params)
  	if params[:add] then
  		@table.table_attrs.build 
  		render action: 'new'
  	elsif params[:remove] then
  		del_id = params[:table][:table_attrs_attributes].keys.max
  		params[:table][:table_attrs_attributes].delete(del_id) if del_id.to_i > 2
  		@table = Table.new(table_params)
  		render action: 'new'
  	elsif params[:confirm] then
	  	if @table.save
	  		@table.table_attrs.where("attr_type = ?", 'Список').each do |a|
	  			@table.properties << Property.find_by(name: a.name)
	  			a.property = Property.find_by(name: a.name)
	  		end
	  		redirect_to @table
	  	else
	  		flash[:alert] = "Вы ввели неверные данные"
	  		render action: 'new'
	  	end
	else
		render action: 'new'
	end
  end

  def show
  	@table = Table.find(params[:id])
  	@attrs = @table.table_attrs
  	
  	@row_count = @attrs.first.rows.size
  end
  
  def destroy
		Table.find(params[:id]).destroy
		redirect_to manage_catalog_path
  end

  def update
  	@table = Table.find(params[:id])
  	params[:table][:table_attrs_attributes].each_value do |avalue|
  		avalue[:rows_attributes].each_value do |rvalue|
  			if !rvalue[:value].is_a?(String) && !rvalue[:value].nil? then
  				dir = Rails.root.join('app', 'assets', 'images', @table.name)
  				Dir.mkdir(dir) unless Dir.exists?(dir)
    					File.open(Rails.root.join('app', 'assets', 'images', @table.name, rvalue[:value].original_filename), 'wb') do |file|
    						file.write(rvalue[:value].read)
    					end
  				#end
  				rvalue[:value] = rvalue[:value].original_filename
  			end
  		end
  	end
  	@table.update(update_params)
  	redirect_to @table
  end
  
  private
  def table_params
    params.require(:table).permit(:name, table_attrs_attributes: [:name, :attr_type, :visible])
  end
  def update_params
  	params.require(:table).permit(table_attrs_attributes: [:id, rows_attributes: [:id, :value, :row]])
  end
 end
