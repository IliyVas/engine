class FeaturesTemplatesController < ApplicationController
	before_action :check_user_role
	layout 'admin'

  def index
  end

  def new
  	@template = FeaturesTemplate.new
  	render 'form'
  end

  def create
  	@template = FeaturesTemplate.new
  	set_params
  	redirect_to features_templates_path
  end

  def edit
  	respond_to do |format|
  		@template = FeaturesTemplate.find(params[:id])
  		format.html { render 'form' }
  		format.js
  	end
  end

  def update
  	@template = FeaturesTemplate.find(params[:id])
  	set_params
  	render nothing: true
  end

  def destroy
  	FeaturesTemplate.find(params[:id]).destroy  	
  	redirect_to features_templates_path
  end

  private

  	def set_params
  		@template.name = params[:features_template][:name]
  		@template.save
		features = params[:features]
		if features
			feature_order = 1
			template_features_ids = []
			features.each do |feature_id, feature_params|
				unless feature_params[:name].blank?
					feature = Feature.find_or_create_by(name: feature_params[:name], basic: (feature_params[:basic].to_bool))
					template_feature = @template.template_features.find_or_initialize_by(feature_id: feature.id)
					template_feature.ordering = feature_order
					template_feature.template_values.clear
					feature_order += 1
					values = feature_params[:values]
					value_order = 1
					values.each do |value_id, value_params|
						if !value_params[:text].blank? || value_params[:picture] || value_params[:pic_id]
							same_text_values = feature.text_values.where(text: value_params[:text])
							values_with_picture = same_text_values.joins(:picture)

							if value_params[:pic_id]
								picture = Picture.new(image: Picture.find_by(id: value_params[:pic_id]).image)
							elsif value_params[:picture]
								picture = Picture.new(image: value_params[:picture]) 
							end

							if picture
								value = values_with_picture.take

								unless value && value.picture.image_fingerprint == picture.image_fingerprint
									picture.save
									value = feature.text_values.new(text: value_params[:text])
									value.picture = picture
									feature.save
								end

							else
								value = same_text_values.where.not(id: values_with_picture.pluck('text_values.id')).take
								value = feature.text_values.create(text: value_params[:text]) unless value
							end
							template_value = template_feature.template_values.find_or_initialize_by(text_value_id: value.id)
							template_value.ordering = value_order
							value_order += 1
						end		
						template_feature.save	
						template_features_ids.push template_feature.id			
					end 
				end
			end
			@template.template_features.where.not(id: template_features_ids).each {|f| f.destroy}
		end
  	end
end
