class NewsController < ApplicationController
	before_action :check_user_role, except: [:index, :show]
	def new
		@news_item = NewsItem.new
		@action = :create
		render partial: 'news_item_form'
	end

	def edit
		@news_item = NewsItem.find(params[:id])
		@action = :update
		render partial: 'news_item_form'
	end

	def create
		news_item = NewsItem.create(news_params)
		@edit_on = true
		render partial: 'news_item', locals: {news_item: news_item}
	end

	def update
		news_item = NewsItem.find(params[:id])
		news_item.update(news_params)
		@edit_on = true
		render partial: 'news_item', locals: {news_item: news_item}
	end

	def destroy
		NewsItem.find(params[:id]).destroy
		render nothing: true
	end

	def index
		@news = NewsItem.all
		@edit_on = params[:edit] and current_user.try(:admin?)
		render layout: 'admin' if @edit_on
	end	

	def show
		@news= NewsItem.all.order(created_at: :desc)
	end

	private
		def news_params
			params.require(:news_item).permit(:headline, :description, :image, :content)
		end
	
end
