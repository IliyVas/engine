# -*- encoding : utf-8 -*-
class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  helper :application
  before_filter :return_unavailable_status, :presenter_and_cart_init
   

  def title(page_title)
    content_for(:title) { page_title }
  end

  def check_user_role
    unless current_user.try(:admin?)
			flash[:alert] = I18n.t("devise.failure.invalid_role")
      redirect_to root_path
    end
  end

  protected

   def cart_init
    if session[:cart_id] && (cart = Cart.find_by(id: session[:cart_id]))
      cart
    else
      Cart.new
    end
  end

    def return_unavailable_status      
      if CONFIG[:maintenance]
        unless user_signed_in? or params[:controller] == 'subscribers' or params[:controller] == 'devise/sessions'
          render 'static_pages/maintenance', :layout => 'simple', :status => :service_unavailable
        end
      end
    end

    def presenter_and_cart_init
      #reset_session
      @presenter = GeneralContentPresenter.new
      @cart = cart_init
    end
end
