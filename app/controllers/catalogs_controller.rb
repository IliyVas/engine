class CatalogsController < ApplicationController
	before_action :check_user_role
	def create
		@category = Category.create(category_params)
		@category.update_attributes(prev_id: 0, last: true, order: Category.maximum(:order) + 1)
		render layout:false
	end
	
	def show
		@table = Table.find_by(name: params[:name])
		@attr = @table.table_attrs.where("visible != ?", '1')
		@values = Row.joins(:table_attr).where("table_id = ?", Table.find_by(name: params[:name]).id)
		
	end
	
	def destroy
		Category.find(params[:id]).destroy
		render nothing: true
	end
	
	def update
		if params[:order_change]
			prev = params[:prev]
			last = params[:last]
			params[:order].each_with_index do |id, index|
				Category.find(id).update(order: index, prev_id: prev[index], last: last[index])
			end
			render nothing: true
		else
			Category.find(params[:category][:id]).update(category_params)
			render text: params[:category][:name]
		end
	end

	def get_form
		@category = params[:id] ? Category.find(params[:id]) : Category.new
		render layout: false
	end

	def edit
		render layout:'application1'
	end
	
		private
  		def category_params
   			params.require(:category).permit(:name, :label, :hidden, :special, :visible, :title, :product_prefix, :description)
 		end
end
