class GeneralContentsController < ApplicationController
	def update
		content = GeneralContent.find(params[:id])
		content.update(content: params[:content])
		render nothing: true
	end
end
