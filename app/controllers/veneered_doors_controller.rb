class VeneeredDoorsController < ApplicationController
	before_action :check_user_role
	def index
		if ! params[:product].nil?
			params[:product].delete_if {|key, value| value.blank? }
			@list = VeneeredDoor.where(params[:product])
		else 
			@list = VeneeredDoor.all
		end
	end
end
