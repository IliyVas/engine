class PropertiesController < ApplicationController
before_action :check_user_role
  def new
  	@property = Property.new
  	if params[:val_num].nil?
  		@property.property_values.build
  	else
  		params[:val_num].to_i.times { @property.property_values.build }
  	end
  end
  
  def create
  	@property = Property.new(property_params)
  	if params[:add] then
  		@property.property_values.build
  		render action: 'new'
  	elsif params[:remove] then
  		del_id = params[:property][:property_values_attributes].keys.max
  		params[:property][:property_values_attributes].delete(del_id) if del_id.to_i > 0
  		@property = Property.new(property_params)
  		render action: 'new'
  	else
	  	if @property.save
	  		redirect_to @property
	  	else
	  		redirect_to new_property_path
	  	end
	end
  end

  def show
      if params[:property].nil?
          @property = Property.find(params[:id])
      else
          redirect_to Property.find(params[:property][:id]) #I should think about this
      end
  end
  
  def update
    @property = Property.find(params[:id])
    
    @property.update(update_params)
    redirect_to @property
  end
	private
		def property_params
   			params.require(:property).permit(:name, property_values_attributes: [:value])
  		end
  	def update_params
        params.require(:property).permit(:name, property_values_attributes: [:id, :value])
      end
end
