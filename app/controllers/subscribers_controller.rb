# -*- encoding : utf-8 -*-
class SubscribersController < ApplicationController
	def create
		respond_to do |format|
			format.json {
				if Subscriber.find_by(email: params[:new_mail]).present?
					render :json => { success: false, msg: 'Запись с таким адресом у нас уже есть' }
				else
					new_mail = Subscriber.new(email: params[:new_mail])
					if new_mail.save
						render :json => { success: true }
					else
						render :json => { success: false, msg: 'Введите корректный адрес'}
					end
				end
			}
		end
	end
end