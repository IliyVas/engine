class SlidesController < ApplicationController
	before_action :check_user_role
	layout 'admin'
	def new
		@slide = Slide.new
		@slide_presenter = Slide::Presenter.new(@slide)
		render 'slide_form'
	end

	def edit
		@slide = Slide.find_by(order: params[:id])
		@slide_presenter = Slide::Presenter.new(@slide)
		render 'slide_form'
	end

	def create
		slide = Slide.new(order: Slide.count + 1)
		slide.set_data(params[:slide][:data])

		case params[:pattern]
		when 1 
			header = slide.slide_items.new
			header.item_type = 'text'
			header.data = Marshal::dump({position: '50,100', in: 'top', delay: '200'})
			header.style = {width: 600}
			header.content = params[:header]

			text = slide.slide_items.new
			text.item_type = 'text'
			text.data = Marshal::dump( {position: '125,100', in: 'top'})
			text.style = {width: 600}
			text.content = params[:text]

			button = slide.slide_items.new
			button.item_type = 'button'
			button.data = Marshal::dump( {position: '200,100', in: 'top'})
			button.style = {width: 600}
			button.content = params[:button]



			text = params[:text]
			button_text = params[:button_text]
			button_link = params[:button_link]
			image = params[:image]


		else
			slide_items = JSON.parse params[:slide_items]
			slide_items.symbolize_keys!
			slide_items.each do |key, value|
				value.symbolize_keys!
				slide_item = slide.slide_items.new
				slide_item.item_type = value[:item_type]

				slide_item.data = Marshal::dump value[:data]
				slide_item.style = value[:style]
				case slide_item.item_type
				when 'text'
					slide_item.content = value[:content]
				when 'button'
					slide_item.content = Marshal::dump value[:content]
				when 'image'
					slide_item.image = params[:image_items][key]
				end
			end
		end
		slide.save			

		render nothing: true
	end
	def update
		render nothing: true
	end

	def destroy
		slide = Slide.find_by(id: params[:id])
		slide.destroy if slide
		render nothing: true
	end
end
