# -*- encoding : utf-8 -*-
class InfoPagesController < ApplicationController
    before_action :check_user_role, except: [:show]
    def admin_index
        @info_category = params[:info_category]
        @info_presenter = InfoPage::Presenter.new(params[:info_category])
        render layout: 'admin'
    end
    
    def new
        @subcategory = params[:subcategory]
        @page = InfoPage.new(info_category: params[:info_category], subcategory: @subcategory)
        @info_presenter = InfoPage::Presenter.new(params[:info_category])
        @action = :create
        @method = :post
        @class= 'new_info_page'
        @id= 'new_info_page'
        @remote = false
        render layout: 'admin'
    end

    def create
        page = InfoPage.new(page_params)
        if page.subcategory.blank?
            page.order = -InfoPage.count - 1
        else   
            page.order = InfoPage.count + 1
        end
        page.save
        redirect_to admin_info_pages_path(:info_category => page.info_category)
    end

    def edit
        @page = InfoPage.friendly.find(params[:id])
        @subcategory = nil
        @info_presenter = InfoPage::Presenter.new(@page.info_category)
        @action = :update
        @method = :patch
        @class= 'edit_info_page'
        @id= 'edit_info_page_' + params[:id]
        @remote = true
        render :new, layout: 'admin'
    end

    def update
        if params[:gauger_commit]
            client = {first: params[:first], second: params[:second],
             phone: params[:phone], email: params[:email], comment: params[:comment]}
            Mailer.gauger(client).deliver
        else
            page = InfoPage.friendly.find(params[:id])
            page.slug = nil
            page.update(page_params)
        end
        render nothing: true
    end

    def update_subcategory_order
        require 'json'
        ids = []
        order = 0
        hash_array = JSON.parse params[:json]
        subcategory = nil
        info_category = params[:category]
        hash_array.each do |hash|
            hash.symbolize_keys!
            if hash[:subcategory]
                subcategory = hash[:subcategory]
                if hash[:children]
                    hash[:children].each do |c|
                        c.symbolize_keys!
                        page = InfoPage.find(c[:id])
                        ids.push page.id
                        if page.order != order || page.subcategory != subcategory
                            page.update_attributes(order: order, subcategory: subcategory)
                        end
                        order += 1
                    end
                end
            else
                page = InfoPage.find(hash[:id])
                ids.push page.id
                if page.order != order || page.subcategory != subcategory
                    page.update_attributes(order: order, subcategory: subcategory)
                end
                order += 1
            end
        end
        InfoPage.find_each(:conditions => ['info_category = ? AND id NOT IN (?)', info_category, ids.empty? ? '' : ids], &:destroy)
        render nothing: true
    end

    def destroy
        page = InfoPage.friendly.find(params[:id])
        info_category = page.info_category
        page.destroy
        redirect_to info_pages_path(:info_category => info_category)
    end

    def show
       @page = InfoPage.friendly.find(params[:id])
       @page_presenter = InfoPage::Presenter.new(@page.info_category)
    end
      
    private
        def page_params
            if params[:info_page]
                content = params[:info_page][:content]
                params[:info_page][:content] = content.gsub('fake_form', 'form') if content
            end
            params.require(:info_page).permit(:title, :content, :info_category, :subcategory, :page_description, :keywords)
        end
end
