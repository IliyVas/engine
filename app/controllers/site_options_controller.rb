class SiteOptionsController < ApplicationController
	before_action :check_user_role
	def update
		params[:site_options].each do |key, value|
			CONFIG[key] = (value == '1' ? true : false)
		end
		CONFIG.symbolize_keys!
		File.open(Rails.root.join('config','config.yml'), 'w') { |f| YAML.dump(CONFIG, f) }    

		render nothing: true
	end
end
