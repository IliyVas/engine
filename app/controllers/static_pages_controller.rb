# -*- encoding : utf-8 -*-
class StaticPagesController < ApplicationController
  def home
  	@mp_presenter = MainPagePresenter.new   
  end
  
  def maintenance
    render layout: 'simple'
  end
  def callback
    client = {name: params[:name], phone: params[:phone], time: params[:time]}
    Mailer.callback(client).deliver
    render nothing: true
  end
  def gauger
    client = {first: params[:first], second: params[:second],
     phone: params[:phone], email: params[:email], comment: params[:comment]}
    Mailer.gauger(client).deliver
    render nothing: true
  end
  def xml
    require 'pricelist'
    render xml: Pricelist::generate
  end
end
