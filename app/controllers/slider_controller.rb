class SliderController < ApplicationController
	before_action :check_user_role
	layout 'admin'
	def edit
		@slider = Slider::Presenter.new
	end

	def update
		slider_on = params[:site_options][:slider].to_bool
		unless CONFIG[:slider] == slider_on
			CONFIG[:slider] = slider_on
			File.open(Rails.root.join('config','config.yml'), 'w') { |f| YAML.dump(CONFIG, f) } 
		end
		params[:slider].each do |option, value|
			Slider.find_or_create_by(option: option).update_attributes(value: value)
		end
		if params[:slide]
			ids = Array.new
			order = 1
			params[:slide].each do |id, values|
				slide = SimpleSlide.find_by id: id
				if slide
					if values[:image] || slide.order != order || slide.link != values[:link]
						slide.order = order
						slide.link = values[:link]
						slide.image = values[:image] if values[:image]
						slide.save
						order += 1
					end
					ids.push slide.id
				else
					if values[:image]
						slide = SimpleSlide.new(link: values[:link], image: values[:image], order: order)
						slide.save
						order += 1
						ids.push slide.id
					end
				end
			end
		end
		SimpleSlide.where.not(id: ids).each {|s| s.destroy }
		render nothing: true
	end

end
