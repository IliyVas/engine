class ProductInCartController < ApplicationController
	def create
		@new_product_product_in_cart = ProductsInCart.new(chosen_params)
		new_product = true
		cart = cart_init
		if cart.new_record?
			cart.save
			cookies[:cart_id] = cart.id
		else
			if (pr = cart.products_in_cart.where(product_id: @new_product_product_in_cart.product_id)).any?
				pr.each do |product|
					size = product.chosen_values.size - 1
					product.chosen_values.each_with_index do |val, index|
						val_exist = false
						@new_product_product_in_cart.chosen_values.each do |new_val|
							if val.text_value_id == new_val.text_value_id && val.picture_id == new_val.picture_id && val.complectation_id == new_val.complectation_id
								val_exist = true
							end
						end
						if val_exist 
							if index == size
								product.quantity += 1
								product.save
								new_product = false
								break
							end
						else
							break
						end
					end
					break unless new_product						
				end
			end
		end
		if new_product
			@new_product_product_in_cart.quantity = 1
			@new_product_product_in_cart.save
			cart.products_in_cart << @new_product_product_in_cart
		end
		render partial: 'sum_and_count'
	end

	def destroy
		ProductsInCart.find(params[:id]).destroy
		render partial: 'sum_and_count'
	end

	private
		def chosen_params
			params.require(:products_in_cart).permit(:product_id, chosen_values_attributes: [:text_value_id, :picture_id, :complectation_id])
		end
end
