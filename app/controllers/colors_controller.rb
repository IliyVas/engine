class ColorsController < ApplicationController
before_action :check_user_role
  def show
  	@color = Color.new
  end
  def create
  	@color = Color.new(color_params)
  	if @color.save
  		redirect_to color_path
  	else
  		render action: 'show'
  	end
  end	
  	private
  		def color_params
  			params.require(:color).permit(:name, :value)
  		end
end
