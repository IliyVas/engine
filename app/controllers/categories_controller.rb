class CategoriesController < ApplicationController
	before_action :check_user_role
	layout 'admin'

	def search
		@categories = Category.where("name LIKE ?", '%'+params[:search]+'%')
		@categories = @categories.where.not(id: params[:ids])
    	render partial: 'products/category_tr', layout: false, locals: {categories: @categories} 
    end

	def new
		@category = Category.new(prev_id: params[:prev_id])
		render 'form'
	end

	def create
		category = Category.new(category_params)
		category.last = true
		category.features_template = FeaturesTemplate.find_by(id: params[:features_template])

		category.save
		Category.find(category.prev_id).update_attributes(last: false) if category.prev_id
		redirect_to edit_catalog_path
	end

	def edit
		@category = Category.find(params[:id])	
		render 'form'
	end	

	def update
		category = Category.friendly.find(params[:id])
		category.update(category_params)
		category.features_template = FeaturesTemplate.find_by(id: params[:features_template])
		category.save

		render nothing: true
	end
	  
	  private
  		def category_params
   			params.require(:category).permit(:name, :label, :hidden, :special, :visible, :title, :product_prefix,
   			 :description, :prev_id, :meta_description, :keywords)
 		end
end
