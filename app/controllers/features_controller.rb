class FeaturesController < ApplicationController
	before_action :check_user_role
	layout 'admin'

	def search 
		feature = Feature.where("name LIKE ?", params[:name]+'%')
		render json: feature.to_json(:only => [:id, :name, :basic], :include => {:text_values => {:only => [:id, :text], :include => {:picture => {only: [:id], :methods => :small_url}}}})
	end

	def search_values
		feature = Feature.find_or_initialize_by(name: params[:name], basic: params[:basic].to_bool)
		text = params[:text] || ''
		values = feature.text_values.where("text LIKE ?", text+'%')
		render json: values.to_json(:only => [:id, :text], :include => {:picture => {only: [:id], :methods => :small_url}})
	end
	
	def edit
		@feature = Feature.find(params[:id])
		render 'form'
	end

	def new
		@feature = Feature.new
		render 'form'
	end

	def update
		feature = Feature.update(params[:id],feature_params)
		render nothing: true
	end

	def update_order
		order = params[:order]
		basic = params[:basic]
		Feature.where.not(id: order).each {|f| f.destroy}
		order.each_with_index {|val, index| Feature.find(val).update_attributes(order: index, basic: (basic[index] == 'true'))}
		render nothing: true
	end

	def create
		feature = Feature.new(feature_params)
		feature.save
		redirect_to features_path
	end

	def destroy
		Feature.find(params[:id]).destroy
		redirect_to features_path
	end

	private
		def feature_params
			params.require(:feature).permit(:name, :basic, :searchable, text_values_attributes: [:id, :text, :_destroy, picture_attributes: [:id, :image]])
		end
end
