class CatalogController < ApplicationController
	before_action :check_user_role
	layout 'admin'
	def edit
		@categories = Category.all
	end

	def update
		require 'json'
		hash = JSON.parse params[:json]
		@ids= []
		save_order(hash)
		Category.where.not(id: @ids).each {|c| c.destroy}
=begin
		order = params[:order]
		prev = params[:prev]
		root_category = Category.find_by(prev_id: nil)
		Category.where.not(id: order).includes(:products).find_each do |cat|
			cat.products.find_each do |prod|
				if prod.category_id == cat.id
					prod.category = root_category
					prod.save
				end
			end
			root_category.products << cat.products
			cat.destroy
		end

		order.each_with_index do |val, index|
			cat = Category.find(val)
			last = !prev.include?(cat.id.to_s)
			cat.update_attributes(order: index, last:last, prev_id: (prev[index] == "" ? nil : prev[index]))

		end
=end
		render nothing: true
	end

  private

  	def save_order(hashes_array, prev_id=nil)
  		existed_ids = []
  		hashes_array.each_with_index do |hash, order|
  			hash.symbolize_keys!
  			category = Category.find_by(id: hash[:id])
  			@ids.push(category.id)
			if hash[:children]
				save_order(hash[:children],category.id)
				last = false
			else
				last = true
			end

  			category.update_attributes(order: order, prev_id: prev_id, last: last) 
  				unless category.order == order and category.prev_id == prev_id and category.last == last 
  		end
  	end
  end
end