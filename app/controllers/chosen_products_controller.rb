class ChosenProductsController < ApplicationController
	def create
		respond_to do |format|
			format.js {
				@cart.chosen_products.new(chosen_params)
				
				@isSaved = @cart.save
				session[:cart_id] = @cart.id unless session[:cart_id] if @isSaved
				render 'refresh_cart'
			}
			format.any { render nothing:true}
		end
	end

	def destroy
		respond_to do |format|
			format.js {
				if (chosen_product = ChosenProduct.find_by(id: params[:id]))
					chosen_product.destroy
				end
				render 'refresh_cart'
			}
			format.any { render nothing:true}
		end
	end

	private
		def chosen_params
			params.require(:chosen_product).permit(:product_id, :quantity, :complectation_id, :current_price, chosen_values_attributes: [:text_value_id])
		end
end
