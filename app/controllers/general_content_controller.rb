class GeneralContentController < ApplicationController
	before_action :check_user_role
	def update
		GeneralContent.update_content(params[:general_content])
		render nothing: true
	end
end
