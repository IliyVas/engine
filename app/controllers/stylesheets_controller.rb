class StylesheetsController < ApplicationController
	before_action :check_user_role
	def index
		template = File.load(Rails.root.join('app').join('views').join('stylesheets').join('style.scss'))
		sass_engine = Sass::Engine.new(template, {
      :syntax => :scss,
      :cache => false,
      :read_cache => false,
      :style => :compressed
    })
		output = sass_engine.render
		logger.debug '!!!!!!!!!!!!!!!!!!!!!!!'
		logger.debug output
    end

    def style
	    render_to_string 'style',
	      formats: [:scss],
	      layout:  false,
	      locals:  { topbar_bg: (cookies[:topbar_bg] ? cookies[:topbar_bg] : '#777') }
	end
end