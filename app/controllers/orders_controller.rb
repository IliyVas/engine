# -*- encoding : utf-8 -*-
class OrdersController < ApplicationController
  before_action :check_user_role, except: [:create, :show, :pdf]
  layout 'admin', only: [:index, :edit]
  def index
    fields = ['id', 'first', 'second' , 'email']
    search_params = params[:orders_search].split(' ') if params[:orders_search]
    if search_params.nil?
      @orders = Order.all
    elsif search_params.length > 4
      @orders = Order.none
    else
      where = where_statement(fields, search_params.length)
      @orders = Order.where(where, {:first => '%' + (search_params[0] ? search_params[0] : '') + '%',
                                   :second => '%' + (search_params[1] ? search_params[1] : '') + '%',
                                   :third => '%' + (search_params[2] ? search_params[2] : '') + '%',
                                   :forth => '%' + (search_params[3] ? search_params[3] : '') + '%'} )
    end
    respond_to do |format|
      format.html {
      }
      format.js {
        @catalog_hide = true
        render layout: false  
   }
    end
  end
  def update 
    Order.find(params[:id]).update(order_params)
    render nothing:true
  end

  def destroy
    Order.find(params[:id]).destroy
    if Order.all.empty?
      render text: 'У вас нет заказов в настоящий момент'
    else
      render nothing:true
    end
  end
  def create
    order = Order.new
    order.chosen_products = @cart.chosen_products
    order.payment_status = 'Не оплачено'
    order.status = 'В обработке'
    if order.update(order_params)
      @cart.chosen_products.delete_all
      if session[:orders]
        session[:orders].push order.id
      else
        session[:orders] = [order.id]
      end
      Mailer.new_order(order).deliver
  	  redirect_to order
    else
      flash[:create_order_error] = "При попытке создания заказа произошла ошибка,
      но вы всегда можете заказать обратный звонок (или позвонить самостоятельно) и
      сделать заказ по телефону."
      redirect_to '/cart'
    end
  end

  def show
    if session[:orders].include? params[:id].to_i
      @order = Order.find(params[:id])
    else
      redirect_to root_path
    end
  end

  def edit
    @order = Order.find(params[:id])
  end

  def pdf
    if session[:orders].include? params[:id].to_i
      pdf_filename = File.join(Rails.root, 'orders_pdf/Заказ_' + params[:id] + '.pdf')
      send_file(pdf_filename, :filename => "Заказ.pdf", :disposition => 'inline', :type => "application/pdf") 
    else
      redirect_to root_path
    end    
  end

  private
  	def order_params
  		params.require(:order).permit(:first, :second, :email, :phone, :delivery,
       :payment_method,:status, :payment_status, :comment, :address,
        chosen_products_attributes: [:id, :quantity])
  	end

    def array_for_where(fields, deep, stack=Array.new)      
      if deep == 0
        return stack
      elsif fields.size == 1
        return stack.push(fields.pop)
      else
        arr = Array.new
        for field in fields do
          stack.push(field)
          fields_copy = fields.dup
          fields_copy.delete(field)
          if fields_copy.size == 1 || deep == 1
            arr.push(array_for_where(fields_copy, deep-1, stack.dup))
          else
            arr.push(*array_for_where(fields_copy, deep-1, stack.dup))
          end
          stack.pop
        end
        return arr
      end
    end

    def where_statement(fields, deep)
      where = ''
      arr = array_for_where(fields, deep)
      puts arr.to_s
      arr.map! do |sub_arr|
        
        sub_arr.each_with_index { |value, index| sub_arr[index] = value + ' LIKE ' + symb(index+1)}
        where << ( '(' + sub_arr.join(' AND ') + ')')
        where << ' OR ' if sub_arr != arr.last
      end
      where
    end

    def symb(num)
      case num
      when 1
        ':first'
      when 2
        ':second'
      when 3
        ':third'
      when 4
        ':forth'
      end
    end
end
