# -*- encoding : utf-8 -*-
class ProductsController < ApplicationController
	before_action :check_user_role, except: [:show, :index, :search]
	helper_method :sort_column, :sort_direction

	def admin_index
		respond_to do |format|
			format.html { @products = Product.paginate(:page => params[:page], :per_page => params[:per_page])
				render layout: 'admin' }
			format.js { 
				if params[:category_id].blank?
					@products = Product.where('products.name LIKE ?', "%#{params[:name]}%")
				else
					name = params[:name] || ''
					@products = Product.where('products.name LIKE ? AND category_id = ?', '%'+ name +'%', params[:category_id])
				end
				column = case params[:sort]
				when 'category'
					'categories.name'
				when 'price'
					'case when new_price is not null and new_price > 0 then new_price else price end'
				else
					sort_column
				end
				@products = @products.includes(:category) if params[:sort] == 'category'
				@products = @products.order(column + ' ' + sort_direction).paginate(:page => params[:page], :per_page => params[:per_page])
			}		
		end
	end

	def hide_product
		product = Product.find(params[:id])
		if params['catalogPage'].to_bool
			product.update_attributes visible: false
		else
			product.update_attributes on_main_page: false
		end
		render nothing: true
	end

	def new
		id = params[:id]
		if id
			@product = Product.find(id)
			@modifications = @product.modifications
			@modifications.push @product
		else
			@product = Product.new
			@modifications = @product.modifications
		end
		@action = :create
		@method = :post
		@class= 'new_product ws-validate'
		@id= 'new_product'
		@remote = false
		render 'form', layout: 'admin'
	end

	def show
		@product = Product.friendly.find(params[:id])
        @product_presenter = Product::Presenter.new(@product)
        @chosen_product = ChosenProduct.new
	end

	def edit
		@product = Product.friendly.find(params[:id])
		@modifications = @product.modifications
		@action = :update
		@method = :patch
		@class= 'edit_product ws-validate'
		@id= 'edit_product_' + params[:id]
		@remote = true
		render 'form', layout: 'admin'
	end

	def update
		@product = Product.friendly.find(params[:id])
		@product.slug = nil
		@product.update(product_params)
		@product.features.clear
		@product.text_values.clear
		set_params
		@product.save
		respond_to do |format|
			format.js
			format.any { render nothing: true }
		end
	end

	def search
		respond_to do |format|
			name = params[:search] ? params[:search] : ''
			format.html { @products = Product.where("name LIKE ?", '%'+name+'%').paginate(:page => params[:page], :per_page => params[:per_page]) }
			format.js { 
				column = case params[:sort]
				when 'price'
					'case when new_price is not null and new_price > 0 then new_price else price end'
				else
					sort_column
				end
				@products = Product
				.where('products.name LIKE ?', "%#{params[:search]}%")
				.order(column + ' ' + sort_direction)
				.paginate(:page => params[:page], :per_page => params[:per_page])
			}		
		end
	end

	def admin_search
		@products = Product.where("name LIKE ?", '%'+params[:search]+'%')
        if type = params[:search_type]
            @products = @products.where.not(id: params[:ids])
            render partial: 'product_tr', layout: false, locals: {products: @products, product_type: type} 
        end
	end

	def create
		@product= Product.new(product_params)
		set_params
		redirect_to edit_product_path(@product)	
	end

	def index
		@category = Category.friendly.find(params[:id]) if params[:id]
	end

	def batch_action
		@action = params[:products_action]
		case @action
		when 'move'				
			category_id = params[:category_to_products_move_in]
			Product.where(id: params[:checked_products])
			.update_all(category_id: category_id)
			@category = Category.find(category_id)
		when 'delete'
			Product.where(id: params[:checked_products]).find_each(&:destroy)
		end
	end

	def destroy
		Product.friendly.find(params[:id]).destroy
		respond_to do |format|
			format.html { redirect_to admin_products_path }
			format.js
		end
	end

	private 

		def product_params
			params.require(:product).permit(:category_id, :name, :prefix, :price, :short_description, :keywords, :description, :basic_image, :on_main_page,
			 :new_price, :visible, :additional_pictures_attributes => [:id, :image, :_destroy], 
			 :complectations_attributes => [:id, :name, :price,:new_price, :description, :standart, :order, :_destroy])
		end

		def set_params
		
		params[:product][:categories].delete('')
		categories = Category.where(id: params[:product][:categories])
		@product.categories = categories unless @product.persisted? and @product.categories == categories
		features = params[:features]
		feature_order = 1
		if features
			features.each do |feature_id, feature_params|
				unless feature_params[:name].blank?
					feature = Feature.find_or_create_by(name: feature_params[:name], basic: (feature_params[:basic].to_bool))
					values = feature_params[:values]
					value_order = 1
					values.each do |value_id, value_params|
						if !value_params[:text].blank? || value_params[:picture] || value_params[:pic_id]
							
							same_text_values = feature.text_values.where(text: value_params[:text])
							values_with_picture = same_text_values.joins(:picture)

							if value_params[:pic_id]
								picture = Picture.new(image: Picture.find_by(id: value_params[:pic_id]).image)
							elsif value_params[:picture]
								picture = Picture.new(image: value_params[:picture]) 
							end

							if picture
								value = values_with_picture.take

								unless value && value.picture.image_fingerprint == picture.image_fingerprint
									picture.save
									value = feature.text_values.new(text: value_params[:text])
									value.picture = picture
									feature.save
								end

							else
								value = same_text_values.where.not(id: values_with_picture.pluck('text_values.id')).take
								value = feature.text_values.create(text: value_params[:text]) unless value
							end
							product_value = @product.products_values.find_or_initialize_by(text_value_id: value.id)
							product_value.ordering = value_order
							value_order += 1
						end
					end
					product_feature = @product.features_products.find_or_initialize_by(feature_id: feature.id)
					product_feature.ordering = feature_order
					feature_order += 1 
				end
			end
		end

		@product.related_products = Product.where(id: params[:related_products])
		@product.related_categories = Category.where(id: params[:related_categories])
		@product.save

		new_mod_ids = params[:modifications] || []
		all_new_ids = new_mod_ids.push @product.id
		all_new = Product.where(id: all_new_ids)
		all_old_ids = @product.modifications.pluck('products.id').push @product.id

		(all_old_ids - all_new_ids).each {|id| Product.find(id).modifications.delete(all_new) }
		all_new_ids.each {|id| Product.find(id).modifications = Product.where(id: all_new_ids.dup.reject{ |e| e==id} )}
		standart_comp = @product.complectations.find_by(standart: true)
		standart_comp.update_attributes(price: @product.price,
			new_price: @product.new_price) if standart_comp
	end

	def sort_column
		if params[:action] == 'search'
			%w[price created_at].include?(params[:sort]) ? params[:sort] : 'id'
		else
			%w[name category price visible].include?(params[:sort]) ? params[:sort] : 'name'
		end
	end

	def sort_direction
		%w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
	end

	def set_modifications
		
	end
		
end
