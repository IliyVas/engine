$.fn.previewImg = (img) ->
	$input = $(this)
	if ($input.prop('files') && $input.prop('files')[0])
		reader = new FileReader()
		reader.onload = (e) -> 
			img.attr('src', e.target.result)
		reader.readAsDataURL($input.prop('files')[0])

$.fn.removePopup = ->
	$popup = $(this)
	setTimeout(
		->
			$popup.fadeOut(2600)
			$popup.queue("fx", ->
				$popup.remove()
			)
		2000)
scrollToEl = (element) ->
	offset = element.offset()
	offsetTop = offset.top
	$('body,html').animate({
		scrollTop: offsetTop
    }, 500)

successPopup = ->
	$popup = $('<div class="ajax-popup success">Изменения сохранены</div>')
	$('#inform_popups').append($popup)
	$popup.slideDown()
	$popup.hover( 
		-> 
			$(this).clearQueue()
			$(this).stop()
			$(this).css('opacity', 100)
		-> 
			$(this).removePopup()
		)
	$popup.removePopup();

errorPopup = ->
	$popup = $('<div class="ajax-popup error">Ошибка</div>')
	$('#inform_popups').append($popup)
	$popup.slideDown()
	$popup.hover( 
		-> 
			$(this).clearQueue()
			$(this).stop()
			$(this).css('opacity', 100)
		-> 
			$(this).removePopup()
		)
	$popup.removePopup()

add_ajax_events = ->
	objects = '.general-content-edit, .product-page-content, #subcategory_tree_form, 
	.main-bottom form, .tree-edit form, .slide-form, .edit_order, .edit_features_template,
	.edit_feature, .edit_category, .edit_order, .edit_info_page, .form-group:last, .slider-edit, .gc-edit,
	.edit-cart-content, .edit-footer-content, .gc-edit, .c-edit'
	only_err = ', #batch_action, #new_product, .edit_product, .switch-form, .form-group'

	$(objects).bind 'ajax:success', ->
		successPopup()
		shadeRemove()
		
		
	$(objects + only_err).bind 'ajax:error', ->
		error = true
		errorPopup()
		shadeRemove()

products_ui = ->
	$ui = null
	$hiddingAllowed = true
	figure = null
	$('#main figure').hover( 
		->
			figure = this
			$ui = $(this).find('.products-ui')
			$ui.animate({opacity: 100}, {duration: 100, queue: false})
		(e) ->
			newEl = e.toElement || e.relatedTarget
			if newEl == null or !(newEl.className == 'product-ui' and newEl.parentNode == figure)
				$ui.animate({opacity: 0}, {duration: 100, queue: false})
		)
	

admin_menu = ->
	$am = $('#admin_menu')
	if $am[0]
		if $(window).width()-1200 > 490 && $am
			$('#am_label').hide()
			$am.css('left', $('#main_container').position().left + $('#main_container').width() + 30 )
		else
			$am.hover(
				->
					$(this).stop().animate({ left: ($(window).width() - 250) }, 400)
				->
					$(this).stop().animate({ left: $(window).width() }, 400)
			)
			$am.css('left', $(window).width() )



imagesPreview = ->
	$field = $('#product_basic_image')
	$img = $('#basic_img_preview')
	if !$img.attr('src')
		$img.hide()
	$field.change( -> 
		$img = $('#basic_img_preview')
		$(this).previewImg($img)
		if $img.css('display') == 'none'
			$img.slideDown()
		)

openCurrentSubmenu = ->
	path = window.location.pathname
	search = window.location.search
	$ul = $('#admin_panel').children('ul').find('ul:has([href="'+path+search+'"])')
	$ul.show()
	$ul.prev().addClass('open')

admin_ready = ->
	modificationIds = []
	CKEDITOR.disableAutoInline = true;
	add_ajax_events()
	products_ui()
	admin_menu() if $('#admin_menu')[0]
	openCurrentSubmenu()
	imagesPreview()

$(document).on 'click', 'html', (evt) ->
	$('.panel-button .dropdown').each( ->
		$dropdown = $(this)
		if $dropdown.css('display') == 'block'
			$dropdown.slideUp(60)
		)
	
$(document).on 'click', '.panel-button .open-dropdown', (evt) ->
	$this = $(this)
	if !$this.hasClass('disabled')
		$dropdown = $this.next()
		$dropdown.slideDown(60)
	evt.stopPropagation()

$(document).on 'click', '.open-dropdown a', ->
	$(this).closest('.dropdown').slideUp(60)

error = false
$(document).on 'click', '.submit', (evt) ->
	error = false
	$this = $(this)

	confirmation = $this.data('confirmation')
	if confirmation
		if !confirm(confirmation) then return

	form = $this.data('form')

	for instance of CKEDITOR.instances
		CKEDITOR.instances[instance].updateElement()

	$radioButton = $this.find('input:radio')
	if $radioButton[0]
		$radioButton.prop('checked', true)
		console.log $radioButton.prop('checked')
	if form is undefined
		form = $this.closest('form')
	else
		form = $(form)
	form.checkValidity()
	form.submit()
	$('.item-to-delete').remove()


$(document).on 'click', '.inline-content', (evt) ->
	if evt.target.nodeName != 'YMAPS' && 
	evt.target.nodeName != 'INPUT' && 
	evt.target.nodeName != 'TEXTAREA'
		$div = $(this)
		$textArea = $(this).prev()
		$div.hide();
		$textArea.show().val($div.html())
		CKEDITOR.inline($textArea[0], {
			toolbar: $textArea.data('toolbar'),
			on: {
				'destroy': (evt) -> 
					$div.show().html($textArea.val())
			}
		})

$(document). on 'click', '#admin_panel .has-child', (evt) ->
	$this = $(this)
	$links = $this.add($this.closest('ul').prev()).add($this.next().find('.has-child'))
	$next = $this.next()
	if $next.is('ul')
		if $next.css('display') == 'none'
			$this.addClass('open')
			$next.slideDown()
			$('.open').not($links).removeClass('open').next().slideUp()
		else
			$this.removeClass('open')
			$next.slideUp()

$(document).on 'click', '#add_news_item', (evt) ->
	$.ajax '/admin/news/new',
	type: 'GET'
	dataType: 'html'
	complete: (res1, s) ->
		$('#news_first_block').after(res1.responseText)
		$news_item = $('.news-item:first')
		$news_item.find('form').bind 'ajax:complete', (s, res2) ->
			successPopup()
			shadeRemove()
			$news_item.replaceWith(
				'<div class="block">
					<div class="section">' + res2.responseText +
					'</div>
				</div>')
			$('#no_news').remove()
		$news_item.find('form').bind 'ajax:error', (s, res2) ->
			errorPopup()
			shadeRemove()


$(document).on 'click', '.edit-news-item', (evt) ->
	$news_item = $(this).closest('.news-item')
	$.ajax '/admin/news/' + $(this).data('id') + '/edit',
	type: 'GET'
	dataType: 'html'
	complete: (res1, s) ->
		$old = $news_item.after(res1.responseText)
		$news_item = $old.next()
		$old.remove()
		$news_item.find('form').bind 'ajax:complete', (s, res2) ->
			$news_item.replaceWith(res2.responseText)


$(document).on 'click', '.delete-news-item', (evt) ->
	$news_item = $(this).closest('.news-item')
	$.ajax '/admin/news/' + $(this).data('id'),
	type: 'DELETE'
	error: (res, s) ->
		errorPopup()
		shadeRemove()
	success: (res, s) ->
		$news_item.remove()
		successPopup()
		shadeRemove()
		if !$('.news-item')[0]
			$('m-header').after('<h3 id="no_news">Пока не добавлено ни одной новости</h3>')



$(document).on 'click', '.add-field', (evt) ->
	new_id	= new Date().getTime()
	regexp = new RegExp($(this).data('association')+"_attributes\\]\\[\\d+", "g")
	data = $(this).data('fields').replace(regexp, ($(this).data('association')+'_attributes]['+ new_id))
	if $(this).data('association') == 'complectations'
		$(this).closest('.f-complectations').append(data)
	else
		$(this).closest('tr').after(data)
	$('#new_id_here').attr('id', $('#new_id_here').closest('tr').find('input').attr('name'))
	$(".file-field").change( ->
		readURL(this)
	)
$(document).on 'click', '.delete-field', (evt) ->
	$(this).closest('.f-complectation, .f-feature').remove()

$(document).on 'click', '.delete-modification', (evt) ->
	$(this).closest('tr').remove()

$(document).on 'click', '.add-feature', (evt) ->
	sel = $('#f_features option:selected')
	$('#f_features').val(sel.next('option').val())
	sel.wrap('<span></span>').parent().hide()
	new_id	= new Date().getTime()
	#	regexp1 = new RegExp("\\[features_attributes\\]\\[\\d+\\]", "g")
	#	regexp2 = new RegExp("\\[text_values_attributes\\]\\[", "g")
	#	regexp3 = new RegExp("\\[pictures_attributes\\]\\[", "g")
	#	res = result.responseText.replace(regexp1, '[features_attributes]['+ new_id + ']')
	#	res = res.replace(regexp2, '[text_values_attributes]['+ new_id)
	#	res = res.replace(regexp3, '[pictures_attributes]['+ new_id)
	$('.f-features').append(sel.data('fields'))

$(document).on 'click', '#feature_categories_open', (evt) ->
	$list = $('#feature_categories')
	if $list.css('display') == 'none'
		$(this).find('a').removeClass('expand').addClass('collapse')
	else
		$list.slideUp()
		$(this).find('a').removeClass('collapse').addClass('expand')


$(window).resize( ->
	$am = $('#admin_menu')
	if $am[0]
		$main = $('#main')
		if $(window).width()-1200 > 490 and $am
			$('#am_label').hide()
			$am.unbind('mouseenter').unbind('mouseleave')
			$am.css('left', $main.position().left + $main.width() + 30 )
		else
			$('#am_label').show()
			$am.css('left', $(window).width())
			$am.hover(
				->
					$(this).stop().animate({ left: ($(window).width() - 250) }, 400)
				->
					$(this).stop().animate({ left: $(window).width() }, 400)
				)
)

$( document ).ajaxSend(( event, jqxhr, settings ) ->
	noShadow = false
	noShadowPaths = ['/site_options', '/chosen_products','/features_templates', '/products/admin_index',
	'/admin/features/search', '/admin/features/search_values', '/products/search',
	'/categories/search', '/products/hide_product', '/callback', '/admin/slider',
	'/admin/general_content', '/admin/news/new', '/admin/orders', '/products/admin_search',
	'/products/batch_action']
	for path in noShadowPaths
		noShadow = true if settings.url.indexOf(path) != -1
	if (settings.url.indexOf('/products') != -1 && settings.type == 'DELETE') ||
	(settings.url.indexOf('/information') != -1 && settings.url == document.location.pathname)
		noShadow = true
	if !noShadow
		$shade = $('<div id="shade" class="ajax-loader"></div>')
		$('body').prepend($shade)
		$shade.addClass('fadeIn animated')
	if settings.url == '/admin/information/update_subcategory_order'
		settings.data += '&json=' + JSON.stringify( $('#tree_editor').sortable('serialize').get()[0]	)	
	if settings.url == '/admin/catalog'
		settings.data += '&json=' + JSON.stringify( $('#catalog_editor').sortable('serialize').get()[0]	)
	if settings.url == '/admin/features/update_order'
		$('.feature-tree').sortable('serialize')
		settings.data += '&' + $.param({order: order, basic: basic})
	)
	




$(document).ready(admin_ready)
$(document).on('page:load', admin_ready)

$(document).on 'click', '.hide-product', ->
	$this = $(this)
	$.ajax '/admin/products/hide_product',
	data: $this.data()
	type: 'PATCH'
	success: ->
		$this.closest('figure').remove()

$(document).on 'click', '#add_phone', (evt) ->
	html = '<div class="edit-phone"><input type="text" name="general_content[phone][]"> <a class="del-phone">&#215;</a></div>' 
	$(this).before(html)

$(document).on 'click', '.del-phone', (evt) ->
	if $('.edit-phone')[1]
		$(this).closest('.edit-phone').remove()

$(document).on 'click', '#save_and_go_away', (evt) ->
	$('#new_product, .edit_product').submit()

$(document).on 'click', '#save_and_stay', (evt) ->
	$("html,body").animate({scrollTop: $('#product_name').closest('fieldset').offset().top}, 1000);
	alert('Продукт сохранен. Теперь измените характеристики')
	$('#product_name').attr('readonly', true)
	$('#new_product, .edit_product').attr('data-remote','true')
	$('#new_product, .edit_product').submit()
	$('#new_product, .edit_product').removeAttr("data-remote")
	$("#new_product, .edit_product").removeData("remote")

$(document).on 'click', '#product_menu a[href]', (evt) ->
	el = $(this).attr('href')
	scrollToEl($(el))

$(document).on 'click', '.order-popup-open', (evt) ->
	popup = $(this).closest('p').next('div')
	position = $(this).position()
	link_center = position.left + ($(this).width()/2)
	popup.css('left',link_center - 180 )
	popup.css('top', position.top - popup.height() - 40)
	$('.order-popup-open').closest('p').next('div').hide(200)
	popup.show(300)

$(document).on 'click', '.order-popup-cancel', (evt) ->
	$(this).closest('div').hide(200)

$(document).on 'click', '.get-info-page-form', (evt) ->
	$.ajax '/information/get_form',
	data: $(this).data()
	type: 'GET'
	complete: (result, s) ->
		$('.info-page-edit').html(result.responseText)
		$('.editable').each( ->
			CKEDITOR.inline( this, {toolbar: $(this).data('toolbar')} )
		)
		$('form').bind 'ajax:complete', (s, result) ->
			if $(this).is('.new_info_page') 
				$('.info-menu-edit').append("<li name='"+result.responseText+"'>
				<span class='move-icon'></span>
				<a class='get-info-page-form' data-id='"+result.responseText+"'>" + $("#info_page_title").val() + "</a></strong>
				</li>")
			if $(this).is(".edit_info_page")
				if $('#info_page_id')[0]
					id = $('#info_page_id').val()
					$('[name='+id+'] > a').text($("#info_page_title").val())
				else
					old_sub_name = $('#info_page_old_sub_name').val()
					new_sub_name = $('#info_page_subcategory').val()
					$('[name="'+old_sub_name+'"]').attr('name', new_sub_name)
					.prev('strong').find('a').removeData('subcategory').data('subcategory', new_sub_name)
					.text(new_sub_name)


$(document).on 'click', '.add-info-subcategory', (evt) ->
	$('.info-menu-edit').prepend("<li>
			<span class='move-icon'></span>
			<strong><a class='no-page-in get-info-page-form' data-subcategory='"+ $("#subcategory").val() + "'>" + $("#subcategory").val() + "</a></strong>
			<ul name='"+ $("#subcategory").val() + "'>
			</ul>
		</li>")

$(document).on 'click', '.info-page-order-save', (evt) ->
	$('.info-menu-edit').sortable("serialize")
	$.ajax '/information/1',
	data: {order, sub, info_category: $(this).data('info_category')}
	type: 'PATCH'
	complete: (result,s) ->
		alert('ok')



fake = ->
	ya_maps()
	$('#select_nav_color').colpick({
		flat:true,
		submit:0})



	# Binding order form ajax complete event
	$('.edit_order').bind 'ajax:complete', ->
		links = $(this).find('.order-popup-open')
		popups = links.closest('p').next('div')
		popups.hide(200)
		selected = popups.find('select')
		links.each( ->
			text = $(this).closest('p').next('div').find('select option:selected').text()
			$(this).text(text)
		)

	$('.info-menu-edit').sortable({
		handle: '.move-icon'
		afterMove: ($placeholder, container) ->
			if $placeholder.next('li')[0] && $placeholder.prev('li')[0]
				if !$placeholder.next('li').attr('name') && !$placeholder.prev('li').attr('name')
					$placeholder.remove()
		pullPlaceholder: false
		isValidTarget: ($item, container) ->
			if $item.attr('name')
				if !$('.placeholder').next().attr('name') && 
				$('.placeholder').parent().is('.info-menu-edit')
					return false
			else 
				if ! container.el.attr('name')
					return false
			true
		serialize: (parent, children, isContainer) ->
			
			if parent.is('li[name]')
				order.push(parent.attr('name'))
				sub.push(parent.closest('ul').attr('name'))
	})