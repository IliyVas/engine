catalogSortable = ->
	$container = null
	$catalog = $('#catalog_editor')
	$catalog.sortable({
		afterMove: ($placeholder, container, $closestItemOrContainer) ->
			$placeholder.text('Переместить сюда')
		onDragStart: ($item, cont, _super, event) ->
			$container = cont.el
			$item.css({
				height: $item.height(),
				width: $item.width()
			})
			$item.addClass("dragged")
			$("body").addClass("dragging")
		onDrop: ($item, cont, _super, event) ->
			$item.removeClass("dragged").removeAttr("style")
			$("body").removeClass("dragging")
			if !$container.children('li')[0]
				$container.parent().find('.collapse').remove()
			$p = cont.el.parent()
			if !$p.find('.collapse')[0] and !cont.el.is('#catalog_editor')
				$p.find('> .line .t-category-name').prepend('<a class="collapse"></a>')
		serialize: ($parent, $children, parentIsContainer) ->
			if $catalog.find('li')[0]
				if parentIsContainer
					if $children[0]
						return [$children]
				else 
					result = $.extend({}, {id: $parent.data('id')})
					if $children[0]
						result.children = $children[0]
					return result
			else return [[]]
	})

subcategorySortable = ->
	$next = null
	$prev = null
	$container = null
	itemIsSubcategory = false
	badPlaceholder = ($placeholder) ->
		if itemIsSubcategory
			$p_next = $placeholder.next()
			if ($p_next[0] and !$p_next.find('ul')[0]) or ($placeholder.parent().data('subcategory')?)
				return true
		else if $placeholder.parent().is('#tree_editor') and !$placeholder.next()[0]
			return true
		return false
	$treeEditor = $('#tree_editor')

	$treeEditor.sortable({
		afterMove: ($placeholder, container, $closestItemOrContainer) ->
			if badPlaceholder($placeholder)
				$placeholder.addClass('bad-placeholder')
				$placeholder.text('Нельзя переместить сюда')
			else
				$placeholder.removeClass('bad-placeholder')	
				$placeholder.text('Переместить сюда')			
		serialize: ($parent, $children, parentIsContainer) ->
			if $treeEditor.find('li')[0]
				if parentIsContainer
					if $children[0]
						return [$children]
				else 
					if $parent.hasClass('tree-info-subcategory')
						result = $.extend({}, {subcategory: $parent.find('ul').data('subcategory') })
						if $children[0]
							result.children = $children[0]
						return result
					if $parent.hasClass('tree-item-wrapper')
						result = $.extend({}, {id: $parent.data('id') })
						return result
			else return [[]]
		onDragStart: ($item, cont, _super, event) ->
			$next = $item.next()
			$prev = $item.prev()
			$container = $item.parent()

			if $item.find('ul')[0]
				itemIsSubcategory = true
			$item.css({
				height: $item.height(),
				width: $item.width()
			})
			$item.addClass("dragged")
			$("body").addClass("dragging")
		onDrop: ($item, cont, _super, event) ->
			$item.removeClass("dragged").removeAttr("style")
			$("body").removeClass("dragging")
			if $('#tree_editor').find('ul')[0]
				if itemIsSubcategory
					if ($item.next()[0] and !$item.next().find('ul')[0]) or
					($item.parent().data('subcategory')?)
						if $next[0]
							$next.before($item)
						else
							$prev.after($item)
				else
					if !$item.next()[0] and $item.parent().is('#tree_editor')
						if $next[0]
							$next.before($item)
						else
							$prev.after($item)
					else
						if !$container.find('li')[0]
							$container.addClass('empty')
						cont.el.removeClass('empty')
			itemIsSubcategory = false

		})
categoryEdit = ->
	$categoryFeatures = $('#c_features')
	$categoryFeatures.chosen({no_results_text: "Ничего не найдено"})
		.on('change', (evt, params) ->
			$this = $(this)
			$opt = $this.find('option[value='+params.selected+']')
			$field = $($this.data('field').replace(/name_to_replace/g, $opt.text()))
			$('#f_category_features').find('tbody').append($field)
			$field.find('input').val(params.selected)
			$opt.prop('disabled', true)
			$opt.prop('selected', false)
			$this.trigger("chosen:updated")
		)

acceptSubcategoryName = ->
	$inp = $('#tree_editor').find('input')
	val = $inp.val()
	$li = $inp.closest('li')
	$li.find('.t-sub-name').html(val)
	$li.find('.add-info-page').attr('href', '/admin/information/new?info_category=' + 
		$('#tree_editor').data('category') + '&subcategory='+val)
	$li.find('ul').data('subcategory', val).attr('data-subcategory', val)


admin_trees_ready = ->
	$('#category_prev_id').chosen({no_results_text: "Ничего не найдено"})
	categoryEdit()
	subcategorySortable() if $('#tree_editor')[0]
	catalogSortable() if $('#catalog_editor')[0]
	$('#f_category_features').sortable({
			containerSelector: 'table',
			itemPath: '> tbody',
			itemSelector: 'tr',
			placeholder: '<tr class="placeholder"><td colspan=0></td></tr>',
			handle: '.order'
		})

$(document).ready(admin_trees_ready)
$(document).on('page:load', admin_trees_ready)

$(document).on 'click', '.delete-c-feature', (evt) ->
	$this = $(this)
	value = $this.closest('tr').find('input[type=hidden]').val()
	$categoryFeatures = $('#category_features')
	$opt = $categoryFeatures.find('option[value='+value+']')
	$opt.prop('disabled', false)
	$categoryFeatures.trigger("chosen:updated")
	$this.closest('tr').remove()

$(document).on 'click', '#add_info_subcategory', (evt) ->
	if $('.empty')[0]
		alert('У вас уже есть пустая категория. Вы можете просто переименовать ее.')
	else
		$li = '<li class="tree-info-subcategory">
					<div class="line">
						<div class="t-subcategory-name">
							<a class="collapse"></a>
							<span class="t-sub-name">Новая категория</span>
						</div>
						<div class="tree-actions">
							<a class="tree-action delete-subcategory">Удалить</a>
							<a class="tree-action change-subcategory-name">Изменить</a>
							<a href="/admin/information/new?info_category=' + $('#tree_editor').data('category') + '&subcategory=Новая категория" class="tree-action add-info-page">Добавить</a>
						</div>
					</div>
					<ul class="empty" data-subcategory="Новая категория">
					</ul>
				</li>'
		$sub = $('#tree_editor').find('li:has(ul)')
		if $sub[0]
			$sub.first().before($li).hide().slideDown()
		else
			$('#tree_editor').append($li)

$(document).on 'click', '.change-subcategory-name', (evt) ->
	$inp = $('#tree_editor').find('input')
	if $inp[0]
		if confirm('Вы уже редактируете элемент. Хотите завершить редактирование и сохранить изменения?')
			acceptSubcategoryName()
		else
			return
	$name = $(this).closest('.line').find('.t-sub-name')
	input = "<input type='text' value='"+$name.html()+"'><a class='t-sub-name-accept'>Ок</a>"
	$name.html(input)
	
$(document).on 'click', '.t-sub-name-accept', (evt) ->
	acceptSubcategoryName()

$(document).on 'click', '#collapse_all', (evt) ->
	$('#tree_editor, #catalog_editor').find('ul').slideUp()
	$('.collapse').removeClass('collapse').addClass('expand')

$(document).on 'click', '#expand_all', (evt) ->
	$('#tree_editor, #catalog_editor').find('ul').slideDown()
	$('.expand').removeClass('expand').addClass('collapse')

$(document).on 'click', '#admin_workspace .expand', (evt) ->
	$(this).closest('li').children('ul').slideDown()
	$(this).removeClass('expand').addClass('collapse')

$(document).on 'click', '.collapse', (evt) ->
	$(this).closest('li').children('ul').slideUp()
	$(this).removeClass('collapse').addClass('expand')

$(document).on 'click', '.delete-subcategory', (evt) ->
	$(this).closest('li').find('li').prependTo('#tree_editor')
	$(this).closest('li').remove()

$(document).on 'click', '.delete-info-page', (evt) ->
	$(this).closest('li').remove()

$(document).on 'click', '.delete-category', (evt) ->
	$li = $(this).closest('li')
	$ul = $li.children('ul')
	if $ul.children('li')[0]
		$li.replaceWith($ul.children('li'))
	else
		$li.remove()