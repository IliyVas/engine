$.fn.underline = ->
	$(this).append('<li id="underline"></li>')
	$underline = $('#underline')
	width = $('.selected-nav-item').width()
	pos = $('.selected-nav-item').parent().position().left
	$underline.width(width)
		.css("left", pos)

	$(this).children().hover(
		->
			$underline.css('background-color','#e5493a')
			$underline.stop().animate({
				left: $(this).position().left,
				width: $(this).find('a').width()
			}, 150)
		->
			$underline.css('background-color','#000')
			$underline.stop().animate({
				left: pos,
				width: width
			}, 100))