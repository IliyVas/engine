(function()
{
	var plugin_name = 'insertbuttons';

	CKEDITOR.plugins.add( plugin_name,
	{
		requires: [ 'dialog', 'fakeobjects' ],
		icons: plugin_name,
		hidpi: false,

		onLoad: function()
		{
			var css = 'img.form-placeholder' +
			'{' +
			'	-moz-box-sizing: border-box;' +
			'	-webkit-box-sizing: border-box;' +
			'	box-sizing: border-box;' +
			'	display: block;' +
			'	width: 100%;'	+
			'	height: 400px;'	+
			'	background: url(' + this.path + 'images/forms.png) ' +
					'no-repeat center center white;' +
			'	border: 1px solid #a9a9a9;' +
			'	min-width: 100px;' +
			'	min-height: 50px;' +
			'	margin: 5px 0 10px 0;' +
			'}';

			CKEDITOR.addCss( css );
		},

		init: function( editor )
		{

			editor.addCommand( 'insertGaugerForm', {
			    exec: function( editor ) {
			    	var html = '<fake_form accept-charset="UTF-8" action="/gauger" class="gauger ws-validate" data-remote="true" method="post">' +
			    		'<div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /></div>'+
			    		'<div class="section">' +
							'<label>Контактная информация</label>' +

							'<input id="first" name="first" placeholder="Ваше имя" required="required" type="text" value="" />' +

							'<input id="second" name="second" placeholder="Ваша фамилия" required="required" type="text" value="" />' + 

							'<input id="phone" name="phone" placeholder="Телефон" required="required" type="text" value="" />' +

							'<input id="email" name="email" placeholder="E-mail" required="required" type="text" value="" />' +

							'<textarea id="comment" name="comment" placeholder="Примечание (опционально)"></textarea>' +
							'<input name="gauger_commit" type="submit" value="Отправить" />' +
						'</div>' +
					'</fake_form>';

					var node = CKEDITOR.dom.element.createFromHtml( html );

			    	editor.insertElement(editor.createFakeElement( node, 'form-placeholder', 'form', true ));
			    }
			});

			var iconcPath = this.path + 'icons/';
			var $form = $(editor.element.$.form);

			editor.addCommand( 'insertOrder', {
			    exec: function( editor ) {
			    	editor.insertHtml('<#order_number>');
			    }
			});

			editor.addCommand( 'insertRouble', {
			    exec: function( editor ) {
			    	editor.insertElement( CKEDITOR.dom.element.createFromHtml( '<span class="rouble"><span>руб.</span></span>' ) );
			    }
			});

			editor.ui.addButton( 'InsertGaugerForm',
			{
				label: 'Вставить форму для вызова замерщика',
				command: 'insertGaugerForm',
				icon: iconcPath + 'insert_form.png'
			} );

			editor.ui.addButton( 'InsertOrder',
			{
				label: 'Вставить номер заказа',
				command: 'insertOrder',
				icon: iconcPath + 'insert_order.png'
			} );

			editor.ui.addButton( 'InsertRouble',
			{
				label: 'Вставить знак рубля',
				command: 'insertRouble',
				icon: iconcPath + 'insert_rouble.png'
			} );
		},

		afterInit: function( editor )
		{
			var dataProcessor = editor.dataProcessor,
				dataFilter = dataProcessor && dataProcessor.dataFilter;

			if( ! dataFilter )
			{
				return;
			}

			var elements = {};

			function parse( el )
			{
				if( el.attributes.action && el.attributes.action.indexOf( 'gauger' ) !== -1 )
				{
					var e = editor.createFakeParserElement( el, 'form-placeholder', 'form', true );
					real = e.attributes['data-cke-realelement'];
					if ( real.indexOf( 'fake_form' ) !== -1) {
						firstDiv = real.indexOf('div');
						lastDiv = real.lastIndexOf('div');
						e.attributes['data-cke-realelement'] = real.substring(0, firstDiv) + 
						'fake_form' + real.substring(firstDiv + 3, lastDiv) + 
						'fake_form' + real.substring(lastDiv + 3);
					}

					return e;
				}

				return null;
			}
			elements[ 'fake_form' ] = parse;
			elements[ 'div' ] = parse;
			dataFilter.addRules( { elements: elements }, 1 );
		}
	} );
} )();