/**
 * @license Copyright (c) 2003-2014, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {

	// Define changes to default configuration here.
	// For the complete reference:
	// http://docs.ckeditor.com/#!/api/CKEDITOR.config

	// Define changes to default configuration here. For example:
  config.language = 'ru';
  // config.uiColor = '#AADC6E';

  /* Filebrowser routes */
  // The location of an external file browser, that should be launched when "Browse Server" button is pressed.
  config.filebrowserBrowseUrl = "/ckeditor/attachment_files";

  // The location of an external file browser, that should be launched when "Browse Server" button is pressed in the Flash dialog.
  config.filebrowserFlashBrowseUrl = "/ckeditor/attachment_files";

  // The location of a script that handles file uploads in the Flash dialog.
  config.filebrowserFlashUploadUrl = "/ckeditor/attachment_files";

  // The location of an external file browser, that should be launched when "Browse Server" button is pressed in the Link tab of Image dialog.
  config.filebrowserImageBrowseLinkUrl = "/ckeditor/pictures";

  // The location of an external file browser, that should be launched when "Browse Server" button is pressed in the Image dialog.
  config.filebrowserImageBrowseUrl = "/ckeditor/pictures";

  // The location of a script that handles file uploads in the Image dialog.
  config.filebrowserImageUploadUrl = "/ckeditor/pictures";

  // The location of a script that handles file uploads.
  config.filebrowserUploadUrl = "/ckeditor/attachment_files";

  // Rails CSRF token
  config.filebrowserParams = function(){
    var csrf_token, csrf_param, meta,
        metas = document.getElementsByTagName('meta'),
        params = new Object();

    for ( var i = 0 ; i < metas.length ; i++ ){
      meta = metas[i];

      switch(meta.name) {
        case "csrf-token":
          csrf_token = meta.content;
          break;
        case "csrf-param":
          csrf_param = meta.content;
          break;
        default:
          continue;
      }
    }

    if (csrf_param !== undefined && csrf_token !== undefined) {
      params[csrf_param] = csrf_token;
    }

    return params;
  };

  config.addQueryString = function( url, params ){
    var queryString = [];

    if ( !params ) {
      return url;
    } else {
      for ( var i in params )
        queryString.push( i + "=" + encodeURIComponent( params[ i ] ) );
    }

    return url + ( ( url.indexOf( "?" ) != -1 ) ? "&" : "?" ) + queryString.join( "&" );
  };

  // Integrate Rails CSRF token into file upload dialogs (link, image, attachment and flash)
  CKEDITOR.on( 'dialogDefinition', function( ev ){
    // Take the dialog name and its definition from the event data.
    var dialogName = ev.data.name;
    var dialogDefinition = ev.data.definition;
    var content, upload;


    if (CKEDITOR.tools.indexOf(['link', 'image', 'attachment', 'flash'], dialogName) > -1) {
      content = (dialogDefinition.getContents('Upload') || dialogDefinition.getContents('upload'));
      upload = (content == null ? null : content.get('upload'));

      if (upload && upload.filebrowser && upload.filebrowser['params'] === undefined) {
        upload.filebrowser['params'] = config.filebrowserParams();
        upload.action = config.addQueryString(upload.action, upload.filebrowser['params']);
      }
    }
  });

  config.extraPlugins = 'submit,imap,insertbuttons';
	// The toolbar groups arrangement, optimized for two toolbar rows.
  config.toolbar_InfoPageEditInline = [
  [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ],
  [ 'Replace', 'Scayt' ],['Indent'],
  '/',
  [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ],
  [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock' ],
  [ 'Link', 'Unlink', 'Anchor' ],
  [ 'Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar'],
  '/',
  [ 'Styles', 'Format', 'Font', 'FontSize' ],
  [ 'TextColor', 'BGColor' ],
  [ 'Maximize', 'ShowBlocks' ],
  ['Imap'], ['InsertRouble'],['InsertGaugerForm'],
  ['Submit', 'Cancel']
];

config.toolbar_InfoPageEdit = [
  [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ],
  [ 'Replace', 'Scayt' ],['Indent'],
  '/',
  [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ],
  [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock' ],
  [ 'Link', 'Unlink', 'Anchor' ],
  [ 'Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar'],
  '/',
  [ 'Styles', 'Format', 'Font', 'FontSize' ],
  [ 'TextColor', 'BGColor' ],
  [ 'Maximize', 'ShowBlocks' ],
  ['Imap'], ['InsertRouble'],['InsertGaugerForm']
];

	config.toolbar_ContentEditInline = [
	[ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ],
	[ 'Replace', 'Scayt' ],['Indent'],
	'/',
	[ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ],
	[ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock' ],
	[ 'Link', 'Unlink', 'Anchor' ],
	[ 'Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar'],
	'/',
	[ 'Styles', 'Format', 'Font', 'FontSize' ],
	[ 'TextColor', 'BGColor' ],
	[ 'Maximize', 'ShowBlocks' ],
  ['Imap'], ['InsertRouble'],
  ['Submit', 'Cancel']
];
  config.toolbar_ContentEdit = [
  [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ],
  [ 'Replace', 'Scayt' ],['Indent'],
  '/',
  [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ],
  [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock' ],
  [ 'Link', 'Unlink', 'Anchor' ],
  [ 'Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar'],
  '/',
  [ 'Styles', 'Format', 'Font', 'FontSize' ],
  [ 'TextColor', 'BGColor' ],
  [ 'Maximize', 'ShowBlocks' ],
  ['Imap'], ['InsertRouble']
];
	config.toolbar_TitleEdit = [
	[ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ],
  [ 'Blockquote', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock' ],
  [ 'Link', 'Unlink'],
	[ 'Styles'], ['Format'], ['Font'], ['FontSize' ],
	[ 'TextColor', 'BGColor' ]
];

   config.toolbar_ButtonEdit = [
  [ 'Bold', 'Italic', 'Subscript', 'Superscript', '-', 'RemoveFormat' ],
  [ 'Blockquote', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock' ],
  [ 'Styles'], ['Format'], ['Font'], ['FontSize' ]
];

  config.toolbar_EmailEdit = [
  [ 'Cut', 'Copy', 'Paste', 'PasteText', '-', 'Undo', 'Redo' ],
  [ 'Replace' ],
  [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ],
  [ 'InsertOrder'],
  '/',
  [ 'Styles', 'Format', 'Font', 'FontSize' ],
  [ 'Link', 'Unlink', 'Anchor' ],
  [ 'Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar'],
  [ 'TextColor', 'BGColor' ]
];

	config.toolbar = [
	[ 'Cut', 'Copy', 'Paste', 'PasteText', '-', 'Undo', 'Redo' ],
	[ 'Replace' ],
	[ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ],
  ['InsertRouble'],
	'/',
	[ 'Styles', 'Format', 'Font', 'FontSize' ],
  [ 'Link', 'Unlink', 'Anchor' ],
	[ 'Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar'],
	[ 'TextColor', 'BGColor' ]
];

	config.autoParagraph = true;
	// Remove some buttons, provided by the standard plugins, which we don't
	// need to have in the Standard(s) toolbar.

	// Se the most common block elements.
	config.format_tags = 'p;h1;h2;h3;pre';

  config.allowedContent = true;

	// Make dialogs simpler.
	config.removeDialogTabs = 'image:advanced;link:advanced';
};
