function ya_maps()
{
	var map_class = 'site_imap_anchor';
	$.getScript('http://api-maps.yandex.ru/2.1/?lang=ru_RU', function() 
	{
		window.ymaps.ready( function()
		{
				var maps = $( '.' + map_class );
				for( var i = 0, n = maps.length; i < n; ++ i )
				{
					var map = maps[ i ];
					replace( map );
				}
		
		} );

		function replace( block )
		{
			var def =
			{
				zoom: 17,
				behaviors: [ 'default', 'scrollZoom' ],
				zoom_control: { left: 5, top: 5 },
				map_tools: { left: 35, top: 5 },
				preset: 'twirl#redStretchyIcon'
			};

			var plugin = block.attributes.getNamedItem('data-plugin').value;
			var cfg = JSON.parse( plugin );
			var coords = [ cfg._lon, cfg._lat ];
			var label = cfg._label;

			if( cfg._name )
			{
				block.setAttribute( 'imap_name', cfg._name );
			}
			if( cfg._width )
			{
				block.style.width = cfg._width;
			}
			if( cfg._height )
			{
				block.style.height = cfg._height;
			}

			block.id = 'ymap' + Math.random( 1, 9999 );
			block.__ymap = new ymaps.Map( block.id,
			{
				center: coords,
				zoom: cfg._zoom || def.zoom,
				behaviors: cfg._behaviors || def.behaviors,
			} );

			block.__ymap.controls.remove('trafficControl')
				.add('routeEditor');


			block.__point = new ymaps.Placemark(coords, {
		        balloonContentBody: cfg._label
		    }, {
		        preset: 'islands#redDotIcon'
		    });

			block.__ymap.geoObjects.add( block.__point );
		}
	});
}