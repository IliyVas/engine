loadError = (xhr, status, error) ->
	alert('Error loading')

formatDate = (date) ->
	dd = date.getDate()
	mm = date.getMonth()+1
	yyyy = date.getFullYear()

	dd = '0'+dd if dd<10
	mm = '0'+mm if mm<10
	
	dd+'/'+mm+'/'+yyyy

charts_ready = ->
	schemeNum = 4
	source = { 
		datafields: [
			{ name: 'Date', map: '[Date]', type: 'date' },	
			{ name: 'Value', type: 'string' }],
		root: "ValCurs",
		record: "Record",
		datatype: "xml"
	}

	$('.chart').each( ->
		$chart = $(this)
		сurrency = $chart.data('сurrency')
		code = $chart.data('code')
		source.url = 'get_xml/' + code

		dataAdapter = new $.jqx.dataAdapter(source, {
			beforeLoadComplete: (records) ->
				i = 0
				sum = 0
				minValue = Number.POSITIVE_INFINITY
				maxValue = Number.NEGATIVE_INFINITY
				length = records.length
				while  i < length
					rec = records[i]
					firstDate = rec.Date if i == 0
					lastDate = rec.Date if i == length-1

					rec.Value = parseFloat(rec.Value.replace(',','.'))
					minValue = rec.Value if rec.Value < minValue
					maxValue = rec.Value if rec.Value > maxValue

					sum = sum + rec.Value
					i++
				mean = sum / length
				records.map (rec) -> rec.Mean = mean

				$chart.jqxChart({description: formatDate(firstDate) + ' - ' + formatDate(lastDate)})

				seriesGroups = $chart.jqxChart('seriesGroups')
				seriesGroups[0].valueAxis.minValue = minValue
				seriesGroups[0].valueAxis.maxValue = maxValue
				$chart.jqxChart('refresh');

				records
			})

		source.datafields[2] = { name: 'Mean', type: 'float'} 
		months = ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек']

		settings = {
	                title: сurrency,
	                enableAnimations: true,
	                enableCrosshairs: true,
	                crosshairsDashStyle: '1,1',
	                crosshairsColor: '#0000FF',
	                showLegend: true,
	                padding: { left: 15, top: 5, right: 30, bottom: 5 },
	                titlePadding: { left: 90, top: 0, right: 0, bottom: 10 },
	                source: dataAdapter,                
	                categoryAxis:
	                    {
	                        textRotationAngle: 0,
	                        dataField: 'Date',
	                        formatFunction: (value)->
	                        	value.getDate() + ' ' + months[value.getMonth()]
	                        toolTipFormatFunction: (value)->
                            	formatDate(value)
	                        showTickMarks: true,
	                        type: 'date',
	                        baseUnit: 'day',
	                        valuesOnTicks: true
                        }
	                colorScheme: 'scheme0' + schemeNum++,
	                seriesGroups:
	                    [
	                        {
	                            type: 'stepline',
	                            valueAxis: {
	                                flip: false,
	                                description: 'Руб.'
	                            },
	                            series: [
	                                { dataField: 'Value', displayText: 'Курс', lineWidth: 3, symbolSize: 9 },
	                                { 
	                                	dataField: 'Mean',
	                                	displayText: 'Среднее',
	                                	toolTipFormatFunction: (value, itemIndex, serie, group, categoryValue, categoryAxis) ->
	                                		'Среднее значение за период: ' + value.toFixed(2)                
	                                	lineWidth: 1,
	                                	symbolSize: 7 }
	                            ]
	                        }
	                    ]
	            }
		$chart.jqxChart(settings)
	)

$(document).ready(charts_ready)
$(document).on('page:load', charts_ready)