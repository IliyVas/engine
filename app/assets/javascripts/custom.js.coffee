searchProduct = ->
	$search = $('#search')
	$form = $search.closest('form')
	$form.keypress( (evt) ->
		if evt.keyCode == 13
			if $.trim($search.val()) == ""
				return false
			else
				return true
		)

$(document).on 'click', '#search_icon', (evt) ->
	$search = $('#search')
	if $.trim($search.val()) != ""
		$search.closest('form').submit()

width = 0
pos = 0
padding = 0
lineHeight = 0
headHeight = 0
fontSize = 16
propertiesValues = ->
	if $(document).width() > 1200
		padding = 30
		lineHeight = 106
		shift = lineHeight - lineHeight/1.7
		headHeight = 144
	else
		padding = 20
		lineHeight = 106
		shift = lineHeight - lineHeight/1.7
		headHeight = 144

underlineAndSmartMenu = ->
	menu = $(".h-nav > ul")
	menu.append('<li id="underline"></li>')
	underline = $('#underline')
	$headerMain = $('#header_main')
	if $headerMain[0]
		headerWidth = $headerMain.width()
		$hnav = $('.h-nav')
		navPos = $hnav.position().left + parseInt($hnav.css('padding-left'))
		
		menu.menuAim({
			activate: (item) ->
				$item = $(item)
				underline.css('background-color','#e5493a')
				underline.animate({
					left: ($item.position().left + padding),
					width: $item.find('a').width()
				}, { duration: 150,  queue: false })
				dropdown = $item.find(".popover")
				if dropdown.hasClass('cat-dropdown')
					position = dropdown.position()
					dropdown.css('left', -position.left - navPos)
						.width(headerWidth) 
				else
				dropdown.clearQueue().slideDown(100)
			deactivate: (item) ->
				underline.css('background-color','#000')

				underline.animate({
					left: pos,
					width: width
				}, { duration: 100,  queue: false })
				$(item).find(".popover").clearQueue().slideUp(100)
			submenuDirection: "below"
		})

callbackAjax = ->
	$popup = $('#callback_popup')
	height = $popup.css('height')
	$form = $popup.find('form')
	$form.bind('ajax:complete', ->
		$popup.removeClass('fadeInDown').addClass('fadeOutUp')
		$popup.removeClass('fadeOutUp').addClass('fadeInDown')
		)
	.bind('ajax:success', ->
		$popup.css('height', 'auto')
		$popup.find('h2').remove()
		$popup.find('form').replaceWith('<p class="c-accepted">Ваша заявка принята. В ближайшее время вам перезвонит наш менеджер.<p>')
		)
		
	$popup.on 'click', '.accept-callback', ->
		$form.submit()


imageHover = ->
	$("#main figure").hover(
		->
			$(this).find(".product-image").animate({ marginTop: "-15px", marginBottom: "+15px" }, { duration: 250,  queue: false })
			$(this).find(".shadow").stop().animate({ height: "34px", opacity: 0.25 }, { duration: 250,  queue: false })
		->
			$(this).find(".product-image").animate({ marginTop: "0px", marginBottom: "0px" }, { duration: 250,  queue: false })
			$(this).find(".shadow").stop().animate({ height: "40px", opacity: 1 }, { duration: 250,  queue: false })
	)

compact_menu = false
$(window).scroll ->
	$body = $('body')
	$header = $('header')
	$h_main = $('#header_main')
	$underline = $('#underline')

	if $(window).scrollTop() >0 && compact_menu == false
		$h_main.animate({
			lineHeight: (lineHeight / (1.7 * fontSize)) + 'rem'
		}, { duration: 150, queue: false })
		$underline.animate({
			top: ((lineHeight / 3.4 + fontSize / 2 + 4) / fontSize) + 'rem'
		}, { duration: 150, queue: false })
		compact_menu = true
	if $(window).scrollTop() == 0 && compact_menu == true
		$h_main.animate({
			lineHeight: (lineHeight / fontSize) + 'rem'
		}, { duration: 150, queue: false })
		$underline.animate({
			top: ((lineHeight / 2 + fontSize / 2 + 4) / fontSize) + 'rem'
		}, { duration: 150, queue: false })
		compact_menu = false

popup = ->
	$popup = $('#popup')
	$('#main figure, .cat-header a, .m-header a').hover(
		->
			if $(this).is('figure')
				$popup.text('Подробнее')
			else
				$popup.text('Перейти')
			$popup.removeClass('fadeOut').addClass('fadeIn animated')
			$(this).mousemove( (event) ->
				$popup.css({'top':event.pageY+15,'left':event.pageX+15})
			)
		->
			$popup.removeClass('fadeIn').addClass('fadeOut')
		) 
		

$(window).resize( ->
		if $(document).width() > 1200
			padding = 30
		else
			padding = 20

		pos = ($('.selected-nav-item').position().left + padding) if $('.selected-nav-item')[0]
		$('#underline').animate({
				left: pos,
				width: width
			}, { duration: 100,  queue: false })
	)

windowLoad = ->
	
	width = $('.selected-nav-item').width()
	pos = ($('.selected-nav-item').position().left + padding) if $('.selected-nav-item')[0]
	$('#underline').width(width)
		.css("left", pos)
	$('.product-image').each( ->
		if $(this).width() > 150
			$shadow = $(this).next()
			$shadow.css('width', '100%')
			$shadow.css('margin-left', '0')
	)
	$('.p-images').find('a').magnificPopup({
		type: 'image',
		zoom: {
			enabled: true, 
			duration: 300,
			easing: 'ease-in-out'},
		gallery:{
			enabled:true,
			tPrev: 'Назад (стрелка влево)',
			tNext: 'Вперед (стрелка вправо)',
			tCounter: '%curr% из %total%'},
		tClose: 'Закрыть (Esc)',
		tLoading: 'Загрузка...',
		image: {
			tError: 'Изображение не может быть загружено'}
		})

ready = ->
	propertiesValues()
	underlineAndSmartMenu()
	searchProduct()
	callbackAjax()
	imageHover()
	popup()
	$('#sb-slider').slicebox({
    orientation : 'v',
    perspective : 1260,
    cuboidsRandom : true,
    disperseFactor : 0,
    colorHiddenSides : '#222',
    sequentialFactor : 150,
    speed : 600,
    easing : 'ease',
    autoplay : true,
    interval: 3000,
    fallbackFadeSpeed : 300
	})
	if $('.site_imap_anchor')[0]
		ya_maps()

$(document).ready(ready)
$(document).on('page:load', ->
	ready()
	windowLoad()
	)
$(window).load(windowLoad)

$(document).on 'click', '#open_popup', ->
	openPopup($(this).data('popup'))
