function openPopup(popupId) {
	var $popup = $(popupId);
	var $shade = $('<div id="shade"></div>');
	$popup.after($shade);
	$shade.addClass('fadeIn animated');
	var $close = $popup.find('.close-popup');
	var $shadeAndClose = $shade.add($close);
	setTimeout( function() {
				$popup.show().addClass('fadeInDown animated');
			},350);
	$shadeAndClose.click( function() {
		closePopup(popupId);
	});
}

function closePopup(popupId) {
	var $popup = $(popupId);
	var $shade = $popup.next();
	var $close = $popup.find('.close-popup');
	$popup.removeClass('fadeInDown').addClass('fadeOutUp');
	setTimeout(function() {
			$shade.removeClass('fadeIn').addClass('fadeOut');
			$shade.remove();
			$popup.hide();
		},200);
	setTimeout( function() {
			$shade.remove();
		},500);
	$popup.find('input').each( function(){ $(this).val(''); });
	$close.unbind('click');

}

function shadeRemove() {
	$shade = $('#shade');
	$shade.removeClass('fadeIn').addClass('fadeOut');
	$shade.remove();
}

function successPopup(text) {
	var $popup = $('<div class="ajax-popup success">'+ text + '</div>')
	$('#inform_popups').append($popup);
	$popup.slideDown();
	$popup.hover( function() { 
			$(this).clearQueue();
			$(this).stop();
			$(this).css('opacity', 100);
		},
		function() { 
			$(this).removePopup();
		})
	$popup.removePopup();
}

function errorPopup(text) {
	var $popup = $('<div class="ajax-popup error">'+ text + '</div>')
	$('#inform_popups').append($popup)
	$popup.slideDown()
	$popup.hover( function() { 
		$(this).clearQueue();
		$(this).stop();
		$(this).css('opacity', 100);
	},
	function() {  
		$(this).removePopup();
	})
	$popup.removePopup();
}