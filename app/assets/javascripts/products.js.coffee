$(document). on 'click', '#quantity_increase', (evt) ->
	$el = $('.quantity-input input')
	$el.val(parseInt($el.val()) + 1)

$(document). on 'click', '#quantity_decrease', (evt) ->
	$el = $('.quantity-input input')
	quantity = parseInt($el.val())
	if quantity > 1
		$el.val(quantity - 1)

$(document).on 'click', '.p-option', (evt) ->
	$this = $(this)
	$this.parent().find('input:radio').prop('checked', true)
	if $this.is('.p-complectation')
		price = $this.find('.c-price').html()
		text = $this.find('.p-complectation-name').text()
		$('#price_value').html(price + '<span class="complectation-label">' + text + '</span>')

$(document).on 'click', '.delete-from-cart', (evt) ->
	$this = $(this)
	$tr = $this.closest('tr')
	$.ajax '/admin/chosen_products/' + $this.data('id'),
	type: 'DELETE'
	success: (result, s) ->
		if !$('#chosen_products tbody').find('tr')[2]
			$('#main').html('<h4>В корзине пока нет товаров.</h4>')
		else
			$tr = $this.closest('tr')
			val = $tr.find('input[type=number]').val()
			price = parseInt($tr.find('.cp-price').text())
			sum = val * price
			$sum = $('#sum')
			sumVal = $sum.text()
			$sum.html($sum.html().replace(/\d+/g, sumVal - sum))
			$tr.remove()
$(document).on 'click', '.expand', (evt) ->
	$this = $(this)
	if $this.hasClass('checked')
		$this.removeClass('checked')
		$this.next().slideUp()
	else
		$this.addClass('checked')
		$this.next().slideDown()

currentVal = null


$(document).on 'input', '.cp-quantity', ->
	$this = $(this)
	$tr = $this.closest('tr')
	val = $this.val()
	price = parseInt($tr.find('.cp-price').text())
	sum = price * val
	$cpSum = $tr.find('.cp-sum')
	cpSum = parseInt($cpSum.text())
	currentVal = cpSum / price
	diff = val - currentVal
	$cpSum.html($cpSum.html().replace(/\d+/g, sum))
	$sum = $('#sum')
	sumVal = parseInt($sum.text())
	$sum.html($sum.html().replace(/\d+/g, sumVal + diff*price))

$(document).on 'change', '.delivery input', ->
	$this = $(this)
	if $this.val() == 'true'
		$('.address').show(100)
	else
		$('.address').hide(100)

ready1 = ->

	if !$('.full-deep')[0]
		path = window.location.pathname
		search = window.location.search
		$ul = $('#catalog').find('ul').not(':has([href="'+path+search+'"])')
		$ul.hide().prev().removeClass('checked')

	$('#basic_img').magnificPopup({type:'image'});
	
	$('.popup-gallery').magnificPopup({
	type: 'image',
	delegate: 'a',
	zoom: {
		enabled: true, 
		duration: 300,
		easing: 'ease-in-out'},
	gallery:{
		enabled:true,
		tPrev: 'Назад (стрелка влево)',
		tNext: 'Вперед (стрелка вправо)',
		tCounter: '%curr% из %total%'},
	tClose: 'Закрыть (Esc)',
	tLoading: 'Загрузка...',
	image: {
		tError: 'Изображение не может быть загружено'}
	})

	
$(document).ready(ready1)
$(document).on('page:load', ready1)

