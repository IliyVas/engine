$(document).on 'click', '.show-form', ->
	$this = $(this)
	$this.next().show()
	$this.hide()

$(document).on 'click', '.change-order-status', ->
	$form = $(this).parent()
	$form.hide().submit()
	$a = $form.prev()
	$a.text($form.find('select').val()).show()

$(document).on 'click', '.delete-order', (evt) ->
	if confirm("Вы уверены, что хотите удалить заказ?")
		tr = $(this).closest('tr')
		$.ajax '/admin/orders/' + $(this).data('id'),
		type: 'DELETE'
		complete: (result, s) -> 
			tr.remove()

$(document).on 'input', '#order_search input', ->
	$(this).closest('form').submit()

$(document).on 'click', '#order_search #search_icon', ->
	$(this).closest('form').submit()

$(document).on 'click', '.am-order-search #search_icon', ->
	$(this).closest('form').submit()


