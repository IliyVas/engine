sliderEditReady = ->
	$('#s_slides').sortable({
			containerSelector: 'table',
			itemPath: '> tbody',
			itemSelector: 'tr',
			placeholder: '<tr class="placeholder"/>',
			handle: '.order'
		})

$(document).ready(sliderEditReady)
$(document).on('page:load', sliderEditReady)

$(document).on 'change', '#s_slides input:file', ->
	$this = $(this)
	$this.previewImg($this.closest('tr').find('img'))

$(document).on 'click', '.change-picture', ->
	$(this).prev().click()

$(document).on 'click', '.delete-slide', ->
	$(this).closest('tr').remove()

$(document).on 'click', '.add-slide', ->
	new_id	= new Date().getTime()
	$tr = $($(this).data('field').replace(/new_id/g, new_id))
	
	$('#s_slides tbody').append($tr)