sliderEditReady = ->
	$('#s_slides').find('input:file').each( ->
		$this = $(this)
		$this.previewImg($this.closest('tr').find('img'))
		)

$(document).ready(sliderEditReady)
$(document).on('page:load', sliderEditReady)

$(document).on 'click', '.change-picture', ->
	$(this).prev().click()

$(document).on 'click', '.delete-slide', ->
	$(this).closest('tr').remove()

$(document).on 'click', '.add-slide', ->
	console.log(1)
	new_id	= new Date().getTime()
	$tr = $($(this).data('field').replace('new_id', new_id))
	
	$('#s_slides tbody').append($tr)
	$tr.find('input:file').previewImg($tr.find('img'))