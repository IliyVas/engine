/**
 * https://github.com/kamens/jQuery-menu-aim
*/
(function($) {

    $.fn.menuAim = function(opts) {
        // Initialize menu-aim for all elements in jQuery collection
        this.each(function() {
            init.call(this, opts);
        });

        return this;
    };

    function init(opts) {
        var $menu = $(this),
            menOffset = $menu.offset(),
            menuHeight = $menu.outerHeight(),
            menuWidth = $menu.outerWidth(),
            activeRow = null,
            mouseLocs = [],
            lastDelayLoc = null,
            timeoutId = null,
            options = $.extend({
                rowSelector: "> li",
                submenuSelector: "li:has(div, ul)",
                submenuDirection: "right",
                tolerance: 30,  // bigger = more forgivey when entering submenu
                enter: $.noop,
                exit: $.noop,
                activate: $.noop,
                deactivate: $.noop,
                exitMenu: $.noop
            }, opts);

        var MOUSE_LOCS_TRACKED = 3,  // number of past mouse locations to track
            DELAY = 300;  // ms delay when user appears to be entering submenu

        /**
         * Keep track of the last few locations of the mouse.
         */
        var mousemoveDocument = function(e) {
                mouseLocs.push({x: e.pageX, y: e.pageY});

                if (mouseLocs.length > MOUSE_LOCS_TRACKED) {
                    mouseLocs.shift();
                }
            };

        /**
         * Cancel possible row activations when leaving the menu entirely
         */
        var mouseleaveMenu = function() {
                if (timeoutId) {
                    clearTimeout(timeoutId);
                }
                loc = mouseLocs[mouseLocs.length - 1];
                var delay = exitDelay();
                if (delay) {
                    timeoutId = setTimeout(function() {
                        mouseleaveMenu();
                    }, delay);
                } else {
                    if (activeRow) {
                        options.deactivate(activeRow);
                    }
                    activeRow = null;
                }
        };

        var mouseenterRow = function() {
                if (timeoutId) {
                    clearTimeout(timeoutId);
                }

                options.enter(this);
                possiblyActivate(this);
            },
            mouseleaveRow = function() {
                options.exit(this);
            };

        var clickRow = function() {
                activate(this);
            };

        var activate = function(row) {
                if (row == activeRow) {
                    return;
                }

                if (activeRow) {
                    options.deactivate(activeRow);
                }

                options.activate(row);
                activeRow = row;
            };

        var possiblyActivate = function(row) {
                var delay = activationDelay();

                if (delay) {
                    timeoutId = setTimeout(function() {
                        possiblyActivate(row);
                    }, delay);
                } else {
                    activate(row);
                }
            };

        var activationDelay = function() {
                if (!activeRow || !$(activeRow).is(options.submenuSelector)) {
                    // If there is no other submenu row already active, then
                    // go ahead and activate immediately.
                    return 0;
                }

                var offset = $(activeRow).find('.popover').offset(),
                    upperLeft = {
                        x: offset.left,
                        y: offset.top - options.tolerance
                    },
                    upperRight = {
                        x: offset.left + $(activeRow).find('.popover').outerWidth(),
                        y: upperLeft.y
                    },
                    lowerLeft = {
                        x: menOffset.left,
                        y: menOffset.top + menuHeight + options.tolerance
                    },
                    lowerRight = {
                        x: menOffset.left + menuWidth,
                        y: lowerLeft.y
                    },
                    loc = mouseLocs[mouseLocs.length - 1],
                    prevLoc = mouseLocs[0];

                if (!loc) {
                    return 0;
                }

                if (!prevLoc) {
                    prevLoc = loc;
                }

                if (prevLoc.x < menOffset.left || prevLoc.x > lowerRight.x ||
                    prevLoc.y < menOffset.top || prevLoc.y > lowerRight.y) {
                    return 0;
                }
                if (lastDelayLoc &&
                        loc.x == lastDelayLoc.x && loc.y == lastDelayLoc.y) {
                    return 0;
                }

                function slope(a, b) {
                    return (b.y - a.y) / (b.x - a.x);
                };

                var decreasingCorner = upperRight,
                    increasingCorner = upperLeft;

                var decreasingSlope = slope(loc, decreasingCorner),
                    increasingSlope = slope(loc, increasingCorner),
                    prevDecreasingSlope = slope(prevLoc, decreasingCorner),
                    prevIncreasingSlope = slope(prevLoc, increasingCorner);

                if (((loc.x-prevLoc.x) > 0 && decreasingSlope < prevDecreasingSlope) ||
                        ((loc.x-prevLoc.x) < 0 && increasingSlope > prevIncreasingSlope)) {
                    lastDelayLoc = loc;
                    return DELAY;
                }

                lastDelayLoc = null;
                return 0;
            };

            var exitDelay = function() { 
                if (!activeRow || !$(activeRow).is(options.submenuSelector)) {
                    // If there is no other submenu row already active, then
                    // go ahead and activate immediately.
                    return 0;
                }
                var loc = mouseLocs[mouseLocs.length - 1],
                    prevLoc = mouseLocs[0];
                
                var offset = $(activeRow).find('.popover').offset(),
                    upperLeft = {
                        x: offset.left,
                        y: offset.top - options.tolerance
                    },
                    upperRight = {
                        x: offset.left + $(activeRow).find('.popover').outerWidth(),
                        y: upperLeft.y
                    };
                if (loc.y > menOffset.top && loc.y < (menOffset.top + menuHeight) 
                    && loc.x > upperLeft.x && loc.x < upperRight.x) {
                    if (!loc) {
                        return 0;
                    }

                    if (!prevLoc) {
                        prevLoc = loc;
                    }

                    if (lastDelayLoc &&
                        loc.x == lastDelayLoc.x && loc.y == lastDelayLoc.y) {
                        return 0;
                    }
                    
                    if ((loc.y-prevLoc.y) >= 0) {
                        lastDelayLoc = loc;

                        return DELAY;
                    }
                }
                lastDelayLoc = null;
                    return 0;
            };

        /**
         * Hook up initial menu events
         */
        $menu
            .mouseleave(mouseleaveMenu)
            .find(options.rowSelector)
                .mouseenter(mouseenterRow)
                .mouseleave(mouseleaveRow)
                .click(clickRow);

        $(document).mousemove(mousemoveDocument);

    };
})(jQuery);

