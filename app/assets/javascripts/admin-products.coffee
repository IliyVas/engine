replaceId = (association, data) ->
	new_id	= new Date().getTime()
	regexp = new RegExp(association + "_attributes\\]\\[\\d+", "g")
	field = data.replace(regexp, association + '_attributes]['+ new_id)
	return field

ids = {modifications: [], related_categories: [], related_products: []}

ajaxAdd = ->
	ids = {modifications: [], related_categories: [], related_products: []}
	ajax = (path, value, type, ids, $tbody) ->
		$.ajax path,
		type: 'GET'
		dataType: 'html'
		data: {search: value, search_type: type, ids: ids}
		complete: (res, s) ->
			$tbody.html(res.responseText)

	$('#add_modification').on('input', (evt) ->
		$this = $(this)
		value = $this.val()
		$tbody = $('#modifications_popup').find('tbody')
		if value.length > 1
			path = '/admin/products/admin_search'
			type = 'modification'
			ajax(path, value, type, ids.modifications, $tbody)
		else
			$tbody.html('')
	)
	$('#add_related_product').on('input', (evt) ->
		$this = $(this)
		value = $this.val()
		$tbody = $('#related_products_popup').find('tbody')
		if value.length > 1
			path = '/admin/products/admin_search'
			type = 'related_product'
			ajax(path, value, type, ids.related_products, $tbody)
		else
			$tbody.html('')
	)
	$('#add_related_category').on('input', (evt) ->
		$this = $(this)
		value = $this.val()
		$tbody = $('#related_categories_popup').find('tbody')
		if value.length > 1
			path = '/admin/categories/search'
			type = 'related_category'
			ajax(path, value, type, ids.related_categories, $tbody)
		else
			$tbody.html('')
	)

features = []
chosenFeatures = []
valuesCache = {}
chosenValues = {}

featureAutocomplete = ($inp) ->	
	currentBasic = null
	chosenFeatures.push($inp.data('id'))

	$inp.autocomplete({
		minLength: 0,
		select: (event, ui) ->
			#Не знаю почему не работает с val()
			setTimeout( ->
				$inp.val(ui.item.name)
			0.5)
			$inp.closest('tr').find('.basic input[type=checkbox]').prop('checked', ui.item.basic)
			chosenFeatures.push(ui.item.id)
			$inp.data('id', ui.item.id)
		source: (request, response) ->
			filter = ->
				matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i")
				response($.grep(features, (value) ->
					return false if value.id in chosenFeatures
					return matcher.test(value.name)
					)
				)

			if features.length == 0
				currentBasic =  $inp.closest('tr').find('.basic input[type=checkbox]').prop('checked')
				$.getJSON( "/admin/features/search", {name: ""}, (data, status, xhr) ->
					for feature in data
						valuesCache[[feature.name, feature.basic]] = feature.text_values
						delete feature.text_values
					features = data
					filter()
					)
			else
				filter()
			
		})
	.focus( ->
		currentBasic = $inp.closest('tr').find('.basic input[type=checkbox]').prop('checked')
		$inp.autocomplete("search", $inp.val())
		)
	.on('input',  ->
		val = $inp.val()
		exist = false
		for feature in features
			if feature.name == val and feature.basic == currentBasic
				exist = true
				if !(feature.id in chosenFeatures)
					chosenFeatures.push(feature.id)
					$inp.data('id', feature.id)
		if !exist
			id = $inp.data('id')
			if id 
				index = chosenFeatures.indexOf(id)
				chosenFeatures.splice(index, 1)
				$inp.removeData('id')
		)
	.autocomplete( "instance" )._renderItem = (ul,item) ->
			return $( "<li>" )
				.append( $( "<a>" ).text( item.name ) )
				.appendTo( ul )

valueAutocomplete = ($inp) ->
	$tr = $inp.closest('.feature-tr')
	current = null
	currentName = $tr.find('.feature-name input[type=text]').val()
	currentBasic = $tr.find('.basic input[type=checkbox]').prop('checked')
	chosenValue = chosenValues[[currentName, currentBasic]]
	id = $inp.data('id')
	if id
		if chosenValue
			chosenValue.push(id)
		else
			chosenValues[[currentName, currentBasic]] = [id]

	$inp.autocomplete({
		minLength: 0,
		select: (event, ui) ->
			name = $tr.find('.feature-name input[type=text]').val()
			basic = $tr.find('.basic input[type=checkbox]').prop('checked')
			
			if ui.item.selectAll
				$tr = $inp.closest('tr')
				for value in valuesCache[[name, basic]]
					regexp = /\]\[\d+\]/g
					if !chosenValues[[name, basic]] || !(value.id in chosenValues[[name, basic]])
						new_id	= new Date().getTime()
						$newTr = $('<tr>' + $tr.html().replace(regexp, ']['+new_id+']') + '</tr>')
						$newTr.insertBefore($tr)
						$textInp = $newTr.find('input[type=text]')
						$textInp.val(value.text).data('id', value.id)
						valueAutocomplete($textInp)
						$imgInp = $newTr.find('.value-picture')
						$pic_id = $imgInp.find('input[type=hidden]')
						picture = value.picture
						if picture
							if $pic_id[0]
								$pic_id.val(picture.id)
							else
								$fileInp = $imgInp.find('input[type=file]')
								newName = $fileInp.attr('name').replace('picture', 'pic_id')
								$hid = $('<input type="hidden" name="' + newName + '" >')
								$hid.appendTo($imgInp).val(picture.id)
								$fileInp.replaceWith( $fileInp = $fileInp.clone( true ) )
								$imgInp.find('a').html('Изменить')
							$imgInp.find('img').attr('src', picture.small_url)
						else
							$pic_id.remove() if $pic_id
							$fileInp = $imgInp.find('input[type=file]')
							$fileInp.replaceWith( $fileInp = $fileInp.clone( true ) )
							$imgInp.find('a').html('Добавить изображение')
							$imgInp.find('img').removeAttr('src')
				$tr.remove()
			else

				setTimeout( ->
					$inp.val(ui.item.text)
				0.5)
				picture = ui.item.picture
				if picture
					$imgInp = $inp.closest('tr').find('.value-picture')
					$hid = $imgInp.find('input[type=hidden]')
					if $hid[0]
						$hid.val(picture.id)
					else
						$fileInp = $imgInp.find('input[type=file]')
						newName = $fileInp.attr('name').replace('picture', 'pic_id')
						$hid = $('<input type="hidden" name="' + newName + '" >')
						$hid.appendTo($imgInp).val(picture.id)
						$fileInp.replaceWith( $fileInp = $fileInp.clone( true ) )
						$imgInp.find('a').html('Изменить')
					$imgInp.find('img').attr('src', picture.small_url)
				values = chosenValues[[name, basic]]
				if values
					values.push(ui.item.id)
				else
					chosenValues[[name, basic]] = [ui.item.id]
				$inp.data('id', ui.item.id)
		response: ( event, ui ) ->
			if $inp.val() == '' && (!chosenValues[[currentName, currentBasic]] || chosenValues[[currentName, currentBasic]].length != valuesCache[[currentName, currentBasic]].length)
				ui.content.unshift {selectAll: true}
		source: (request, response) ->
			filter = ->
				matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i")
				response($.grep(cachedValue, (value) ->
					if chosenValues[[name, basic]]
						return false if value.id in chosenValues[[name, basic]]
					return matcher.test(value.text)
					)
				)
			name = $tr.find('.feature-name input').val()
			basic = $tr.find('.basic input[type=checkbox]').prop('checked')
			if name.trim()
				cachedValue = valuesCache[[name, basic]]
				if cachedValue and cachedValue.length > 0
					filter()
				else
					$.getJSON( "/admin/features/search_values", {name:name, basic:basic, text: ""}, (data, status, xhr) ->
						valuesCache[[name, basic]] = data
						cachedValue = data
						filter()
					)
		})
	.focus( ->
		currentName = $tr.find('.feature-name input[type=text]').val()
		currentBasic = $tr.find('.basic input[type=checkbox]').prop('checked')
		$inp.autocomplete("search", $inp.val())
		)
	.on('input', ->
		
		val = $inp.val()
		cachedValue = valuesCache[[currentName, currentBasic]]
		if cachedValue
			exist = false
			for cachedVal in cachedValue
				if cachedVal.text == val and !cachedVal.picture
					exist = true
					$imgInp = $inp.closest('tr').find('.value-picture')
					$hid = $imgInp.find('input[type=hidden]')
					$fileInp = $imgInp.find('input[type=file]')
					if !$hid[0] and !($fileInp.prop('files') and $fileInp.prop('files')[0])
						$inp.data('id', cachedVal.id)
						values = chosenValues[[currentName, currentBasic]]
						if values 
							if!(cachedVal.id in values)
								values.push(cachedVal.id)
						else
							chosenValues[[currentName, currentBasic]] = [cachedVal.id]
			if !exist
				id = $inp.data('id')
				if id
					values = chosenValues[[currentName, currentBasic]]
					if id in values
						index = values.indexOf(id)
						values.splice(index, 1)
				$inp.removeData('id')
		)
	.autocomplete( "instance" )._renderItem = (ul,item) ->
		return $( "<li>" )
			.append( $( "<a>" ).text( ->
				if item.selectAll
					return 'Добавить все'
				else
					return item.text
				))
			.appendTo( ul )

edit_product_ready = ->
	$('#f_modifications').find('.product-tr').each( ->
		$this = $(this)
		ids.modifications.push($this.data('id'))
		$this.removeClass('product-tr')
	)
	$('#f_related_products').find('.product-tr').each( ->
		$this = $(this)
		ids.related_products.push($this.data('id'))
		$this.removeClass('product-tr')
	)
	$('#f_related_categories').find('.category-tr').each( ->
		$this = $(this)
		ids.related_categories.push($this.data('id'))
		$this.removeClass('category-tr')
	)
	$('#f_features').find('.feature-name input').each( ->
		featureAutocomplete($(this))
		)
	$('#f_features').find('.value input').each( ->
		valueAutocomplete($(this))
		)


admin_product_ready = ->
	features = []
	chosenFeatures = []
	valuesCache = {}
	chosenValues = {}
	$features = $('#f_features')
	$features.on 'change', 'input[type=checkbox]', ->
		$this = $(this)
		$inp = $this.closest('tr').find('.feature-name input')
		val = $inp.val()
		basic = $this.prop('checked')
		exist = false
		for feature in features
			if feature.name == val and feature.basic == basic
				exist = true
				if !(feature.id in chosenFeatures)
					chosenFeatures.push(feature.id)
					$inp.data('id', feature.id)
		if !exist
			id = $inp.data('id')
			if id 
				index = chosenFeatures.indexOf(id)
				chosenFeatures.splice(index, 1)
				$inp.removeData('id')
	
	$features.on "change", 'input[type=file]', ->
		$this = $(this)
		$p = $this.parent()
		$p.find('input[type=hidden]').remove()
		$this.previewImg($p.find('img'))
		$p.find('a').html('Изменить')

		$valTr = $this.closest('tr')
		$tr = $valTr.closest('.feature-tr')
		name = $tr.find('.feature-name input[type=text]').val()
		basic = $tr.find('.basic input[type=checkbox]').prop('checked')
		$textInp = $valTr.find('input[type=text]')
		id = $textInp.data('id')
		if id
			for cachedVal in valuesCache[[name, basic]]
				if cachedVal.id = id
					if !(cachedVal.picture)
						chosen = chosenValues[[name, basic]]
						index = chosen.indexOf(id)
						chosen.splice(index, 1)
		$textInp.removeData('id')
	ajaxAdd()
	edit_product_ready()
	
	$select = $('#admin_products_search_by_category')
	$select.chosen({no_results_text: "Ничего не найдено"})
	$('#category_to_products_move_in').chosen({no_results_text: "Ничего не найдено"})

	
	$('#f_features_templates').chosen({no_results_text: "Ничего не найдено"})
		.on('change', (evt, params) ->
			$.ajax '/admin/features_templates/' + params.selected + '/edit',
			type: 'GET'
			dataType: 'script'
			complete: ->
				$features.find('.feature-name input').each( ->
					featureAutocomplete($(this))
					)
				$features.find('.value input').each( ->
					valueAutocomplete($(this))
					)
				$('#f_features_templates').val('').trigger("chosen:updated")
			)

	$cat_id = $('#product_category_id')
	$cat_id.chosen({no_results_text: "Ничего не найдено"})
		.on('change', (evt, params) ->
			category = $cat_id.find('[value='+params.selected+']')
			$('#product_prefix').val(category.data('prefix'))
			id = category.data('template')
			if id
				$.ajax '/features_templates/' + id + '/edit',
				type: 'GET'
				dataType: 'script'
				complete: ->
					$features.find('.feature-name input').each( ->
						featureAutocomplete($(this))
						)
					$features.find('.value input').each( ->
						valueAutocomplete($(this))
						)
			)
	$tables = $('#f_complectations, #f_modifications, #f_features')
	$tables.each( ->
		$el = $(this)
		$el.sortable({
			containerSelector: 'table',
			itemPath: '> tbody',
			itemSelector: 'tr',
			placeholder: '<tr class="placeholder"/>',
			handle: '.order',
			onDrop: ($item, container, _super, event) ->
				$item.removeClass("dragged").removeAttr("style")
				$("body").removeClass("dragging")
				if $el.is('#f_complectations')
					$inputs = $el.find('tbody').find('.order').find('input')
					i = 1
					for input in $inputs
						$(input).val(i++)
		})
	)
	if document.referrer.indexOf('products/new') != -1
		if (/admin\/products\/\d+\/(edit)$/).test document.location.pathname
			successPopup('Товар сохранен')

$(document).ready(admin_product_ready)
$(document).on('page:load', admin_product_ready)

$(document).on 'submit', '#new_product', ->
	$shade = $('<div id="shade" class="ajax-loader"></div>')
	$('body').prepend($shade)
	$shade.addClass('fadeIn animated')

$(document).on 'click', '.save-modification', ->
	$save = $('#save_changes')
	$save.slideUp().remove()
	$this = $(this)
	$this.removeClass('save-modification').addClass('submit')
	$('[name=_method]').remove()
	$(document).ajaxSend(( event, jqxhr, settings ) ->
		settings.url = '/admin/products'
		)
	$($this.data('form')).submit()

$(document).on 'click', '#add_feature_value', ->
	$tr = $(replaceId('text_values', $(this).data('field')))
	$('#feature_values tbody').append($tr)
	$tr.find('input[type=file]').change( ->
		$this = $(this)
		$this.previewImg($this.parent().find('img'))
	)

$(document).on 'click', '.destroy-feature-value', ->
	$td = $(this).closest('td')
	$tr = $td.closest('tr')
	$tr.addClass('item-to-delete')
	$hid = $td.find('input[type=hidden]')
	$hid.val('1')
	$('#' + $hid.attr('id').replace(/_destroy/g, "id")).addClass('item-to-delete')
	$tr.hide()

$(document).on 'input', '#admin_products_search', ->
	$(this).closest('form').submit()

$(document).on 'change', '#admin_products_search_by_category', ->
	$(this).closest('form').submit()

$(document).on 'click', '#p_reset_fields', ->
	$this = $(this)
	$this.closest('.section').find('input[type=hidden]').val('')
	$('#admin_products_search').val('')
	$('#admin_products_search_by_category').val('').trigger('chosen:updated')
	$this.closest('form').submit()

$(document).on 'click', '#check_all', ->
	$this = $(this)
	if $this.prop("checked") == true 
		$('#products_index').find('input:checkbox').prop('checked', true)
		$('#products_actions').removeClass('disabled')
	else
		$('#products_index').find('input:checkbox').prop('checked', false)
		$('#products_actions').addClass('disabled')

$(document).on 'click', '#product_index_body input:checkbox', ->
	$this = $(this)
	$checkboxes = $('#product_index_body').find('input:checkbox')
	$checkAll = $('#check_all')
	if $this.prop('checked') == true
		allChecked = true
		$checkboxes.each( ->
			if $(this).prop("checked") == false
				allChecked = false
				return false
			)
		if allChecked
			$checkAll.prop("indeterminate", false)
			$checkAll.prop("checked", true)
		else
			$checkAll.prop("indeterminate", true)
		$('#products_actions').removeClass('disabled')
	else
		allUnchecked = true
		$checkboxes.each( ->
			if $(this).prop("checked") == true
				allUnchecked = false
				console.log 2
				return false
			)
		if allUnchecked
			$checkAll.prop("indeterminate", false)
			$checkAll.prop("checked", false)
			$('#products_actions').addClass('disabled')
		else
			$checkAll.prop("indeterminate", true)

$(document).on 'click', '#add_feature', (evt) ->
	new_id	= new Date().getTime()
	$feature = $($(this).data('field').replace(/(?:(features(\[|_)|values\]\[|pictures\]\[))/g, '$&'+new_id))
	$('#f_features').append($feature)
	$featureInp = $feature.find('.feature-name input')
	$valueInp = $feature.find('.value input')
	featureAutocomplete($featureInp)
	valueAutocomplete($valueInp)

$(document).on 'click', '.add-feature-value', (evt) ->
	$this = $(this)
	id = $this.data('id')
	new_id	= new Date().getTime()
	newValue = $('#add_feature').data('new-value')
	$newValue = $( newValue.replace(/features\[/g, (id.replace('_', '['))).replace(/features_/g, id)
		.replace(/(values|pictures)\]\[/g, '$&' + new_id) )
	$this.closest('tr').before($newValue)
	valueAutocomplete($newValue.find('input[type=text]'))

$(document).on 'click', '.delete-feature-value', (evt) ->
	$this = $(this).closest('tr')
	
	if $this.closest('table').find('tr').length > 2
		$inp = $this.find('input[type=text]')
		$tr = $this.closest('.feature-tr')
		name = $tr.find('.feature-name input[type=text]').val()
		basic = $tr.find('.basic input[type=checkbox]').prop('checked')
		id = $inp.data('id')
		if id
			chosen = chosenValues[[name, basic]]
			index = chosen.indexOf(id)
			chosen.splice(index, 1)
		$this.remove()

$(document).on 'click', '.delete-feature', (evt) ->
	$(this).closest('tr').remove()

$(document).on 'click', '.open-file-input', (evt) ->
	$(this).parent().find('input[type=file]').click()

$(document).on 'click', '#open_mod_popup', (evt) ->
	openPopup($(this).data('popup'))

$(document).on 'click', '#modifications_popup .product-tr', (evt) ->
	$this = $(this)
	ids.modifications.push($this.data('id'))
	$this.prependTo('#f_modifications tbody')
	$this.removeClass('product-tr')

$(document).on 'click', '#related_products_popup .product-tr', (evt) ->
	$this = $(this)
	ids.related_products.push($this.data('id'))
	$this.prependTo('#f_related_products tbody')
	$this.removeClass('product-tr')

$(document).on 'click', '.category-tr', (evt) ->
	$this = $(this)
	ids.related_categories.push($this.data('id'))
	$this.prependTo('#f_related_categories tbody')
	$this.removeClass('category-tr')

$(document).on 'click', '.close-popup, #shade', (evt) ->
	$this = $(this)
	if $this.is('#shade')
		$popup = $this.prev()
	else
		$popup = $this.parent()
	$popup.find('tbody').html('') 

$(document).on 'click', '.delete-modification', (evt) ->
	$tr = $(this).closest('tr')
	index = ids.modifications.indexOf($tr.data('id'))
	ids.modifications.splice(index, 1)
	$tr.remove()

$(document).on 'click', '.delete-related-product', (evt) ->
	$tr = $(this).closest('tr')
	index = ids.related_products.indexOf($tr.data('id'))
	ids.related_products.splice(index, 1)
	$tr.remove()

$(document).on 'click', '.delete-related-category', (evt) ->
	$tr = $(this).closest('tr')
	index = ids.related_categories.indexOf($tr.data('id'))
	ids.related_categories.splice(index, 1)
	$tr.remove()

$standartComp = null

$(document).on 'click', '.add-complectation', (evt) ->
	$complectations = $('#f_complectations')
	$complectations.find('tbody').append(replaceId('complectations', $(this).data('field')))
	$comp = $complectations.find('.f-complectation')
	$comp.last().find('.order').find('input').val($comp.length)
	if !$comp[1]
		$standartComp = $('.f-complectation')
		$standartComp.find('input[type="radio"]').prop('checked', true)
		$standartComp.find('.price input').prop('disabled', true).val($('#product_price').val())
		$standartComp.find('.new_price input').prop('disabled', true).val($('#product_new_price').val())
	else if !$comp[2]
		$standartComp.find('.delete-complectation').addClass('disabled')

$(document).on 'input', '#product_price', ->
	$standartComp.find('.price input').val($(this).val()) if $standartComp

$(document).on 'input', '#product_new_price', ->
	$standartComp.find('.new_price input').val($(this).val()) if $standartComp

$(document).on 'click', '.delete-complectation:not(.disabled)', (evt) ->
	$this = $(this)
	$next = $this.next()
	if $next.is('input')
		$next.val('1')
		$('#f_complectations').after($next)
	$this.closest('tr').remove()
	if $('.f-complectation').length == 1
		$standartComp.find('.disabled').removeClass('disabled')

$(document).on 'click', '#f_complectations input[type="radio"]:not([checked])', (evt) ->
	$this = $(this)
	$standartComp.find('.disabled').removeClass('disabled')
	$price = $standartComp.find('.price input')
	$newPrice = $standartComp.find('.new_price input')
	$price.prop('disabled', false).val($price.data('price'))
	$newPrice.prop('disabled', false).val($newPrice.data('price'))
	$standartComp.find('.standart input').not(this).prop('checked', false)
	$standartComp = $this.closest('.f-complectation')
	
	$price = $standartComp.find('.price input')
	$newPrice = $standartComp.find('.new_price input')
	$price.prop('disabled', true).data('price', $price.val()).val($('#product_price').val())
	$newPrice.prop('disabled', true).data('price', $newPrice.val()).val($('#product_new_price').val())
	$standartComp.find('.delete-complectation').addClass('disabled')

$(document).on 'click', '#add_additional_image', (evt) ->
	$this = $(this)
	$noFile = $this.parent().find('.no-file')
	if $noFile[0]
		$inp = $noFile.find('input')
	else
		new_id	= new Date().getTime()
		$inp = $('<input type="file" name="product[additional_pictures_attributes][' + new_id + '][image]">')
		$wrapper = $('<div class="no-file"><a class="delete-additional-image">&#215;</a><img></div>')
		$(this).before($wrapper)
		$wrapper.append($inp)
		$inp.change( ->
			$wrapper.removeClass('no-file').addClass('f-additional-image')
			)
	$inp.click()

$(document).on 'change', '.f-additional-image input[type=file]', (evt) ->
	$this = $(this)
	$(this).previewImg($this.closest('.f-additional-image').find('img'))

$(document).on 'click', '.f-additional-image', (evt) ->
	if !$(evt.target).is('a, input')
		$(this).find('input').click()

$(document).on 'click', '.delete-additional-image', (evt) ->
	$wrapper = $(this).closest('.f-additional-image')
	$wrapper.find('input[type=hidden]').insertBefore($wrapper).val(1)
	$wrapper.remove()