// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require respond
//= require jquery
//= require jquery_ujs
//= require colpick
// require jquery.cslider
//= require jquery.menu-aim
//= require modernizr
//= require custom
//= require products
//= require connect
//= require magnific-popup
//= require turbolinks
//= require hoverizr
// require jquery.fractionslider
//= require assets_path
//= require openPopup
//= require jquery.slicebox
//= require webshims/polyfiller
$.webshims.setOptions('basePath', '/assets/webshims/shims/');
$.webshims.setOptions("forms-ext", {
	"types" : "number"
});
webshim.setOptions('forms', {
    lazyCustomMessages: true
});
$.webshims.polyfill('forms forms-ext');