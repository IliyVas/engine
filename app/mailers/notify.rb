# -*- encoding : utf-8 -*-
class Notify < ActionMailer::Base

  def new_order_mail(order)
  	@order = order
  	Email.all.each do |email|
  		mail(to: email.email, subject: 'Новый заказ')
  	end
  end
end
