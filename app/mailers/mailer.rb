# -*- encoding : utf-8 -*-
class Mailer < ActionMailer::Base
  default from: "dveri-vozim@yandex.ru"

  def new_order(order)
    @order = order
    filename = pdf_create(order)
    attachments[filename] = File.read("orders_pdf/" + filename)

    mail(to: GeneralContent.find_by(name: 'order_emails').content + ',' + order.email, subject: 'Заказ №' + @order.id.to_s + ' принят')
  end

  def callback(client)
  	
  	@text = client[:name] + ' просит перезвонить ему на ' + client[:phone] + '.'
  	@text += ' Клиент оставил следующее пожелание о времени звонка: "' + client[:time] + '".' unless client[:time].blank?
  	
  	mail(to: GeneralContent.find_by(name: 'info_emails').content, subject: 'Обратный звонок')
  end

  def gauger(client)
    @client = client
    mail(to: GeneralContent.find_by(name: 'info_emails').content, subject: 'Заявка. Вызов замерщика.')
  end

  private
    def pdf_create(order)
      filename = "Заказ_" + order.id.to_s + ".pdf"
      Prawn::Document.generate("orders_pdf/" + filename) do
        font_families.update(
          "Times_New_Roman" => {
            :bold => "#{Prawn::BASEDIR}/data/fonts/Times_New_Roman_Bold.ttf",
            :italic => "#{Prawn::BASEDIR}/data/fonts/Times_New_Roman_Italic.ttf",
            :normal  => "#{Prawn::BASEDIR}/data/fonts/Times_New_Roman.ttf" },
          "Arial_Narrow" => {:normal => "#{Prawn::BASEDIR}/data/fonts/Arial_Narrow.ttf"},
          "Calibri" => {
            :normal => "#{Prawn::BASEDIR}/data/fonts/calibri.ttf",
            :bold => "#{Prawn::BASEDIR}/data/fonts/calibrib.ttf"},
          "Rouble" => {:normal => "#{Prawn::BASEDIR}/data/fonts/rouble.ttf"})
      
        font "Calibri", :size => 10
        
        string = "Интернет-магазин <b>\"Двери Возим\"</b> www.dveri-vozim.ru \n
        тел. +7 (495) 942-59-09    +7 (901) 564-41-59 \n
        Офис продаж располагается в Бизнес-Центре по адресу: г. Москва, Хибинский проезд, д.20."
        
        pad_bottom(10) {
          text string, :leading => -5, :inline_format => true
        }
      
        stroke_horizontal_rule
      
        move_down 20
      
        font("Arial_Narrow", :size =>24) do
          text "Заказ №" + order.id.to_s, :align => :center
        end

        move_down 10

        text 'Заказчик: ' + order.second + ' ' + order.first

        move_down 6
        text 'Телефон: ' + order.phone

        move_down 6
        text 'e-mail: ' + order.email

        move_down 6
        text 'Способ оплаты: ' + order.payment_method

        move_down 6
        text 'Примечание заказчика:'

        move_down 6
        text order.comment.blank? ? 'Нет' : order.comment

        move_down 6

        table(order.products_to_array, :width => bounds.width, :cell_style => {:inline_format => true}) do
          row(0).font_style = :bold
        end

        move_down 6

        text 'Общая сумма: ' + order.sum.to_s + "<font name='Rouble'>a</font>", :size => 18, :align => :right, :style => :bold, :inline_format => true

        move_down 10

        if order.delivery
          text 'Доставка согласовывается и оплачивается дополнительно.'
        end

        move_down 10 
        font("Arial_Narrow", :size =>24) do
          text "Спасибо за покупку!", :align => :center
        end
      end
      filename
    end
end
