module HeaderHelper
	def email_link
		mail_to GeneralContent.find_by(name: 'info_emails').content, GeneralContent.find_by(name: 'email_link').content 
	end
end