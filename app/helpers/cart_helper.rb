module CartHelper

	def delivery_info
		delivery_info = GeneralContent.find_by(name: 'delivery_info')
		unless delivery_info.blank?
			content_tag :p, delivery_info.content.html_safe, class: 'after-cb'
		end
	end

	def pickup_info
		pickup_info = GeneralContent.find_by(name: 'pickup_info')
		unless pickup_info.blank?
			content_tag :p, pickup_info.content.html_safe, class: 'after-cb'
		end
	end

	def order_info
		order_info = GeneralContent.find_by(name: 'order_info')
		unless order_info.blank?
			order_info.content.html_safe
		end
	end
end