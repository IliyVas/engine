# -*- encoding : utf-8 -*-
module ApplicationHelper

	def count_products_in_cart
		if @cart.persisted?
			@cart.chosen_products.sum(:quantity)
		else
			0
		end		
	end

	def price_sum
		if @cart.persisted?
			sum = 0
			@cart.chosen_products.each do |p|
				sum += p.quantity * p.current_price
			end
			sum
		else
			0
		end
	end

	def col_num(num)
		case num
		when 1
			1
		when 2
			2
		else
			3
		end
	end

	def subcategories(category, id_array = Array.new)
	    if category.last
	      id_array.push(category.id)
	    else
	      Category.where(prev_id: category.id).each { |cat| subcategories(cat, id_array)}
	      id_array
	    end
	end

	def check_box_value(boolean)
		boolean ? 'Да' : 'Нет'
	end

	def paginate(collection, params= {})
		will_paginate collection, params.merge(:renderer => RemoteLinkPaginationHelper::LinkRenderer)
	end

	def link_to_sort_by(title, column)
  		css_class = column == sort_column ? "sorted #{sort_direction}" : nil
  		direction = column == sort_column && sort_direction == "asc" ? "desc" : "asc"
  		link_to title, params.merge(:sort => column, :direction => direction, :page => nil), {:class => css_class, remote: true}
	end

	def link_to_repaginate(number)
		link_to number.to_s, params.merge(:page => nil, :per_page => (number.is_a?(Numeric) ? number : Product.count)), {remote: true}
	end

	def link_to_add_feature_value(form_helper)
		field = render( partial: 'features/feature_value', locals: {form_helper: form_helper, values: TextValue.new})
		content_tag :a, 'Добавить', data: {field: field.gsub('\"','\'') }, id: 'add_feature_value'
	end
	
	def link_to_add_field(f, feature = nil, association)
		new_object = f.object.class.reflect_on_association(association).klass.new
		fields = if( association == :complectations)
			render(partial: 'products/complectation', locals: {form_helper: f, complectations: new_object })
		else
			content_tag :tr do
				content_tag :td do
					if association == :pictures
						name = 'features['+ feature.id.to_s + '][pictures][]'
						file_field_tag name
					else
						name = 'features['+ feature.id.to_s + '][text_values][]'
						text_field_tag name
					end
				end
			end
		end
		association == :pictures ? fields.sub!("<\/tr>", "<td><img id='new_id_here'><\/td><\/tr>") : fields.sub!("<td", "<td colspan='2'")
		content_tag :a, '+', data: {association: association, fields: fields.gsub('\"','\'')}, class: 'add-field'
	end

	def link_to_add_feature(product = nil)
		if product
			feature = Feature.new
			template_feature = nil
		else
			feature = nil
			template_feature = TemplateFeature.new
			template_feature.build_feature
		end

		field = render partial: 'products/f_feature', locals: {feature: feature, template_feature: template_feature, product: product}
		new_value = render partial: 'products/f_feature_values', locals: {feature_id: '', value:TextValue.new}
		content_tag :a, 'Добавить', data: {field: field.gsub('\"','\''), new_value: new_value.gsub('\"','\'') }, id: 'add_feature'
	end
	def link_to_add_complectation(form_helper)
		new_complectation = form_helper.object.complectations.build
        field = render partial: 'f_complectation', locals: {form_helper: form_helper, complectations: new_complectation}
        content_tag :a, 'Добавить', data: {field: field.gsub('\"','\'')}, class: 'add-complectation'
    end

    def link_to_add_slide
    	field = render partial: 'slide_tr'
    	content_tag :a, 'Добавить', data: {field: field.gsub('\"','\'')}, class: 'add-slide'
    end

	def select_with_data(form_helper)
		arr = Array.new
		Feature.all.each do |feat|
			arr.push([feat.name, feat.id, {data: {fields: (render(partial: 'feature', 
				locals:{form_helper: form_helper, features: [feat]})).gsub('\"','\'')}}])
		end
		arr
	end

	def full_path_array(category_id)
		full_path = Array.new
		category = Category.where(id: category_id)
		begin
			full_path.push(category.take.name)
			category = Category.where(id: category.take.prev_id)
		end until category.empty?
		full_path.reverse!
	end

	def full_category_name(category_id)
		full_path_array(category_id).join(' ')
	end

end
