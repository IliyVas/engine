# -*- encoding : utf-8 -*-
# Load the Rails application.
ENV["RAILS_ENV"] ||= "production"

require File.expand_path('../application', __FILE__)

# Initialize the Rails application.
Mystore::Application.initialize!