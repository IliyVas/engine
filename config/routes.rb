Mystore::Application.routes.draw do
  root  'static_pages#home'

  resources :carts, only: :show
  resources :news, only: [:index, :show]
  
  resources :orders, only: [:create, :show] do 
    get 'pdf/:id', to: 'orders#pdf', as: :pdf, on: :collection
  end
  
  resources :products, only: [:show, :index] do
    get :search, on: :collection
  end
  resources :info_pages, :path => "information", only: :show

  mount Ckeditor::Engine => '/ckeditor'
  devise_for :users
  post '/callback', to: 'static_pages#callback'
  post '/gauger', to: 'static_pages#gauger'
  #get "admin_panel/products"
 
  resources :subscribers

  match '/xml', to: 'static_pages#xml', via: 'get'

  match '/admin', to: 'admin#admin', via: 'get'
  match '/admin/edit/header', to: 'admin#edit_header', via: 'get', as: :edit_header
  match '/admin/edit/footer', to: 'admin#edit_footer', via: 'get', as: :edit_footer
  match '/admin/edit/product_page_content', to: 'admin#edit_product_page_content', via: 'get', as: :edit_product_content
  match '/admin/edit/news', to: 'admin#edit_news', via: 'get', as: :edit_news
  match '/admin/edit/main_page_content', to: 'admin#edit_mp_content', via: 'get', as: :edit_mp_content
  match '/admin/edit/cart_content', to: 'admin#edit_cart', via: 'get', as: :edit_cart_content
  match '/admin/site_options', to: 'admin#site_options', via: 'get', as: :site_options
  match '/admin/edit/catalog_content', to: 'admin#edit_catalog_content', via: 'get', as: :edit_catalog_content
  match '/cart', to: 'carts#show', via: 'get'
  match '/catalog(/:id)', to: 'products#index', via: 'get', as: 'catalog'
  match '/admin/edit/emails', to: 'admin#edit_emails', via: 'get', as: :edit_emails

  scope '/admin' do 
    resource :general_content, :controller => :general_content, only: :update
    resource :site_options, only: :update
    resource :catalog, :controller => :catalog, only: [:edit, :update]
    resource :slider, :controller => :slider, only: [:edit, :update]
    resources :slides
    resources :chosen_products
    resources :features_templates
    resources :news, except: [:index, :show]
    resources :orders, except: [:create, :show]

    resources :categories, except: :index do
      get :search, on: :collection
    end
    resources :features do
      get :search, on: :collection
      get :search_values, on: :collection
    end
    resources :products, except: [:show, :index] do
      get :admin_search, on: :collection
      get :admin_index, as: :admin, on: :collection
      patch :hide_product, on: :collection
      post :batch_action, on: :collection
    end
    resources :info_pages, :path => "information", except: :show do
      patch :update_subcategory_order, on: :collection
      get :admin_index, on: :collection, as: :admin
    end
    
  end

  #scope(:path_names => { :new => "neu", :edit => "bearbeiten" }) do
  #  resources :categories, :path => "kategorien"
  #end
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'
  
  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
